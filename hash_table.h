#ifndef HASH_TABLE_H_
#define HASH_TABLE_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define HASH_SIZE 100

typedef enum scopespace_e {
	programvar, functionlocal, formalarg
}scopespace_e;

typedef struct Variable_t {
	char *name;
	unsigned int scope;
	unsigned int line;
	unsigned int offset;
	enum scopespace_e space;
} Variable;

typedef struct Function_t {
	char *name;
	unsigned int scope;
	unsigned int line;
	unsigned int offset;
	unsigned int iaddress;
	unsigned int locals;
	enum scopespace_e space;
} Function;

typedef enum SymbolTableType {
	GLOBAL_V, LOCAL_V, FORMAL_V, USERFUNC, LIBFUNC
} SymbolTableType;

typedef struct SymbolTableEntry_t {
	int isActive;
	union{
		Variable *varVal;
		Function *funcVal;
	} value;
	enum SymbolTableType type;
} SymbolTableEntry;

typedef struct hBucket {
	SymbolTableEntry* stEntry;
	struct hBucket *next;
} hBucket;

typedef struct hTable {
	unsigned int size;
	hBucket **buckets;
} hTable;

// Function Declarations
SymbolTableEntry* createSTEntry (char*, int, int, SymbolTableType);

int isVariable (SymbolTableEntry*);

char* getSTEName(SymbolTableEntry*);

int getSTEScope(SymbolTableEntry*);

enum scopespace_e getSTESpace(SymbolTableEntry*);

unsigned int getSTEOffset(SymbolTableEntry*);

unsigned int getSTElocals(SymbolTableEntry*);

unsigned int getSTEiaddress(SymbolTableEntry*);

int strHash(const char *);

int createHTable (hTable **);

int destroyHTable (hTable **);

int addToHashTable (hTable *, SymbolTableEntry *);

SymbolTableEntry* searchHashTable(hTable *, char *, int );

SymbolTableEntry* newtemp(hTable *);

int istempname(char*);

void resettemp(void);

void hideScope (hTable *, int );

int printHashTable (hTable *);

void setSpace(SymbolTableEntry*, scopespace_e);

void setOffset(SymbolTableEntry*, unsigned int);

void setAddress(SymbolTableEntry*, unsigned int);

void setLocals(SymbolTableEntry*, unsigned int);

scopespace_e currscopespace(void);

unsigned int currscopeoffset(void);

void inccurrscopeoffset(void);

void resetformalargsoffset(void);

void restorecurrscopeoffset(unsigned);

void resetfunctionlocaloffset(void);


// Variable Declarations
extern hTable *symTable;
extern unsigned int programVarOffset;
extern unsigned int functionLocalOffset;
extern unsigned int formalArgOffset;
extern unsigned int scopeSpaceCounter;

extern int yylineno;

#endif
