#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "hash_table.h"
#include "quads_lib.h"
#include "avm_lib.h"
#include "func_im.h"

#define AVM_NUMACTUALS_OFFSET	+4
#define AVM_SAVEDPC_OFFSET		+3
#define AVM_SAVEDTOP_OFFSET		+2
#define AVM_SAVEDTOPSP_OFFSET	+1

unsigned totalActuals = 0;

char* number_tostring(avm_memcell* c){
	char* buffer = (char*)malloc(16);
	sprintf(buffer,"%g",c->data.numVal);
	return buffer;
}
char* string_tostring(avm_memcell* c){
	return c->data.strVal;
}
char* bool_tostring(avm_memcell* c){
	if (c->data.boolVal == 0){
		return "0";
	}else{
		return "1";
	}
}
char* table_tostring(avm_memcell* c){
	return NULL;
}
char* userfunc_tostring(avm_memcell* c){
	return NULL;
}
char* libfunc_tostring(avm_memcell* c){
	return NULL;
}
char* nil_tostring(avm_memcell* c){
	return NULL;
}
char* undef_tostring(avm_memcell* c){
	return NULL;
}

char* avm_tostring(avm_memcell* m){
	assert(m->type >=0);
	return (*tostringFuncs[m->type])(m);
}

void avm_dec_top(void){
	if (!top){
		fprintf(stderr,"stack overflow\n");
		executionFinished = 1;
	}else{
		--top;
	}
}

void avm_push_envvalue(unsigned val) {
	stack[top].type 		= number_m;
	stack[top].data.numVal	= val;
	avm_dec_top();
}

void avm_callsaveenvironment(void) {
	avm_push_envvalue(totalActuals);
	avm_push_envvalue(pc + 1);
	avm_push_envvalue(top + totalActuals + 2);
	avm_push_envvalue(topsp);
}

userFunc* avm_getfuncinfo(unsigned address){
	userFunc* curr = userFL;
	while(curr != NULL){
		if (getSTEiaddress(curr->sym) ==  address){
			break;
		}
		curr = curr -> next;
	}
	return curr;
	//int i;
	//for(i=0; i<currUserFuncSize; i++){
		//if( getSTEiaddress(userFL[i].sym) == address)
			//return &userFL[i];
	//}
	//return NULL;
}

userFunc* avm_getlibraryfunc(char *id){
	userFunc* curr = userFL;
	while(curr != NULL){
		if (strcmp(getSTEName(curr->sym), id) == 0){
			break;
		}
		curr = curr -> next;
	}
	return curr;
	//int i;
	//for(i=0; i<currUserFuncSize; i++){
		//if( strcmp(getSTEName(userFL[i].sym), id) == 0)
			//return &userFL[i];
	//}
	//return NULL;
}

void execute_call(instruction* instr){
	avm_memcell* function = avm_translate_operand(&instr->arg1, &ax);
	assert(function);
	avm_callsaveenvironment();
	
	switch (function->type){
		case userfunc_m: {
			pc = function->data.funcVal;
			assert(pc < AVM_ENDING_PC);
			assert(code[pc].opcode == funcenter_v);
			break;
		}
		
		case string_m: {
			avm_calllibfunc(function->data.strVal);
			break;
		}
		
		case libfunc_m: {
			avm_calllibfunc(function->data.libfuncVal);
			break;
		}
		
		default: {
			char* s = avm_tostring(function);
			fprintf(stderr,"call: cannot bind '%s' to function!\n", s);
			free(s);
			executionFinished = 1;
		}
	}
}

void execute_funcenter(instruction *instr){
	avm_memcell* function = avm_translate_operand(&instr->result, &ax);
	assert(function);
	assert(pc == function->data.funcVal);	//function address should match PC
	
	totalActuals = 0;
	userFunc* funcInfo = avm_getfuncinfo(pc);
	topsp = top;
	top = top - getSTElocals(funcInfo->sym);
}

unsigned avm_get_envvalue(unsigned i){
	assert(stack[i].type == number_m);
	unsigned val = (unsigned) stack[i].data.numVal;
	assert(stack[i].data.numVal == ((double) val));
	return val;
}

void execute_funcexit(instruction* instr){
	unsigned oldTop = top;
	top = avm_get_envvalue(topsp + AVM_SAVEDTOP_OFFSET);
	pc = avm_get_envvalue(topsp + AVM_SAVEDPC_OFFSET);
	topsp = avm_get_envvalue(topsp + AVM_SAVEDTOPSP_OFFSET);
	
	while(oldTop <= top)	// ignore first
		avm_memcellclear(&stack[++oldTop]);	// garbage collection
		
	avm_memcellclear(&retval);
}

void avm_calllibfunc(char* id){
	userFunc* f = avm_getlibraryfunc(id);
	if (!f){
		fprintf(stderr,"unsupported lib func '%s' called!\n", id);
		executionFinished = 1;
	}else{
		
		topsp = top;
		totalActuals = 0;
		//(*f)();
		if (strcmp(getSTEName(f->sym),"print") == 0){
			libfunc_print();
		}
		if (!executionFinished)
			execute_funcexit((instruction*) 0);
	}
}

unsigned avm_totalactuals(void){
	return avm_get_envvalue(topsp + AVM_NUMACTUALS_OFFSET);
}

avm_memcell* avm_getactual(unsigned i){
	assert (i < avm_totalactuals());
	return &stack[topsp + AVM_STACKENV_SIZE + 1 + i];
}

void libfunc_print (void){
	unsigned n = avm_totalactuals();
	unsigned i;
	for (i=0; i<n; ++i){
		char* s = avm_tostring(avm_getactual(i));
		puts(s);
		free(s);
	}
}


void execute_pusharg(instruction* instr){
	avm_memcell* arg = avm_translate_operand(&instr->arg1, &ax);
	assert(arg);
	
	
	avm_assign(&stack[top], arg);
	++totalActuals;
	avm_dec_top();
}



