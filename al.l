/* User defined libraries */
%{
	#if defined(WIN32)
	#define YY_NO_UNISTD_H
	static int isatty (int i) { return 0; }
	#elif defined(_WIN32_WCE)
	#define YY_NO_UNISTD_H
	static int isatty (void *i) { return 0; }
	#endif
	
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include "quads_lib.h"
	#include "parser.h"

	#define YY_FAIL 1
%}

/* Flex parameters */
/* only one input file */
%option noyywrap yylineno

/* Flex macros */
if			"if"
else		"else"
while		"while"
for			"for"
function	"function"
return		"return"
break		"break"
continue	"continue"
and			"and"
not			"not"
or			"or"
local		"local"
true		"true"
false		"false"
nil			"nil"

equal		\={2}
great_equal	\>\=
less_equal	\<\=
assign		\=
great_than	\>
less_than	\<
plus		\+
plus_1		\+{2}
minus		\-
minus_1		\-{2}
mul			\*
div			[\/]
mod			\%
unequal		\!\=

l_brack		\[
r_brack		\]
l_hook		\{
r_hook		\}
l_parenth	\(
r_parenth	\)
semicolon	\;
comma		\,
colon		\:
dbl_col		\:{2}
dot			\.
dots		\.{2}

id			[a-zA-Z][a-zA-Z_0-9]*
string		\"
comment		"//".*
comments	"/*"
integer		[0-9]+
float		[0-9]+\.[0-9]+
nl			[\n]*
ws			[ \t]*


/* Regular expressions */
%%

{if}			{ return IF; }
{else}			{ return ELSE; }
{while}			{ return WHILE; }
{for}			{ return FOR; }
{function}		{ return FUNCTION; }
{return}		{ return RETURN; }
{break}			{ return BREAK; }
{continue}		{ return CONTINUE; }
{and}			{ return AND; }
{not}			{ return NOT; }
{or}			{ return OR; }
{local}			{ return LOCAL; }
{true}			{ return TRUE; }
{false}			{ return FALSE; }
{nil}			{ return NIL; }

{equal}			{ return EQUAL; }
{great_equal}	{ return GREAT_EQUAL; }
{less_equal}	{ return LESS_EQUAL; }
{assign}		{ return ASSIGN;  }
{great_than}	{ return GREAT_THAN; }
{less_than}		{ return LESS_THAN; }
{plus}			{ return PLUS; }
{plus_1}		{ return PLUS_1; }
{minus}			{ return MINUS;  }
{minus_1}		{ return MINUS_1; }
{mul}			{ return MUL; }
{div}			{ return DIV; }
{mod}			{ return MOD; }
{unequal}		{ return UNEQUAL; }

{l_brack}		{ return L_BRACK; }
{r_brack}		{ return R_BRACK; }
{l_hook}		{ return L_HOOK; }
{r_hook}		{ return R_HOOK; }
{l_parenth}		{ return L_PARENTH; }
{r_parenth}		{ return R_PARENTH; }
{semicolon}		{ return SEMICOLON; }
{comma}			{ return COMMA; }
{colon}			{ return COLON; }
{dbl_col}		{ return DBL_COL; }
{dot}			{ return DOT; }
{dots}			{ return DOTS; }

{id}			{ yylval.strValue = strdup(yytext); return ID; }
{string}		{ 

				int c, i = 0, err = 1;
				while ( (c = input()) != EOF){
					if ( c == '\\') {
						if ( (c = input()) == '\\'){
							yytext[i++] = '\\';
						}else if ( c == 'n' ){
							yytext[i++] = '\n';
						}else if ( c == 't' ){
							yytext[i++] = '\t';
						}else if ( c == '\"' ){
							yytext[i++] = '\"';
						}else{
							fprintf(stderr, 
								"%d: WARNING \\%c is not recognized\n",yylineno,c);
							yytext[i++] = '\\'; 
							yytext[i++] = c;
						}
						
					}else if ( c == '\"' ){
						err = 0;
						break;
					}else{
						yytext[i++] = c;
					}
				}
				if (err == 1) {
					fprintf(stderr, "ERROR in line: %d\n",yylineno);
					//yyterminate();
					return YY_FAIL;
				}
				yytext[i] = '\0';
				
				yylval.strValue = strdup(yytext); return STRING;
			}
{comment}	{	/* yylval.strValue = strdup(&yytext[2]); return COMMENT; */}
{comments}	{
				int c,err = 1, found = 0 , nest = 0;
				int i = 0;
				
				while((c = input())!= EOF){
					yytext[i++] = c;
					if(c == '/'){
						if((c = input())== '*'){
							++found;
							++nest;
							yytext[i++] = c;
						}
						else{
							unput(c);
						}
					}		
					else if(c == '*'){
						if((c = input()) == '/'){
							if(found == 0){
								//only to check that it actually ended
								err = 0;
								yytext[--i] = '\0';
								break;
							}else{
								--found;
								unput(c);
							}
						}
					}
				}
				if(err != 0){
					fprintf(stderr, "ERROR in line: %d\n",yylineno);
					//yyterminate();
					return YY_FAIL;
				}
				/*yylval.strValue = strdup(yytext); return COMMENTS; */
			}
{integer}	{	yylval.intValue = atoi(yytext); return INTEGER; }
{float}		{	yylval.floatValue = strtod (yytext, NULL); return FLOAT;}
{nl}		{	}
{ws}		{	}
<<EOF>>		{	return 0; }
.			{	fprintf(stderr,"%d: WARNING Cannot match character: %s with any rule\n",yylineno, yytext); }

%%
