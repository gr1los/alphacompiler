#ifndef FUNC_IM_H_
#define FUNC_IM_H_
#define AVM_STACKSIZE	4096
#define AVM_STACKENV_SIZE 4

extern unsigned		top;
extern unsigned		topsp;
extern avm_memcell	retval;
extern int			currUserFuncSize;
extern userFunc*	userFL;
extern avm_memcell stack[AVM_STACKSIZE];
extern avm_memcell ax, bx, cx;
extern unsigned char	executionFinished;
extern instruction*	code;
extern unsigned		pc;
extern unsigned		codeSize;
#define AVM_ENDING_PC codeSize


typedef char* (*tostring_func_t) (avm_memcell*);

char* number_tostring(avm_memcell*);
char* string_tostring(avm_memcell*);
char* bool_tostring(avm_memcell*);
char* table_tostring(avm_memcell*);
char* userfunc_tostring(avm_memcell*);
char* libfunc_tostring(avm_memcell*);
char* nil_tostring(avm_memcell*);
char* undef_tostring(avm_memcell*);

static tostring_func_t tostringFuncs[] = {
	number_tostring,
	string_tostring,
	bool_tostring,
	table_tostring,
	userfunc_tostring,
	libfunc_tostring,
	nil_tostring,
	undef_tostring,
};

char* avm_tostring(avm_memcell*);

void			avm_dec_top(void);
void			avm_push_envvalue(unsigned);
void			avm_callsaveenvironment(void);
userFunc*		avm_getfuncinfo(unsigned);
userFunc*		avm_getlibraryfunc(char*);

void			execute_call(instruction*);
void			execute_funcenter(instruction*);
unsigned		avm_get_envvalue(unsigned);
void			execute_funcexit(instruction*);
void			avm_calllibfunc(char*);
unsigned		avm_totalactuals(void);
avm_memcell*	avm_getactual(unsigned);
void			libfunc_print (void);
void			execute_pusharg(instruction*);

#endif
