#ifndef TABLE_IM_H_
#define TABLE_IM_H_

extern avm_memcell ax, bx, cx;
extern unsigned char	executionFinished;
extern unsigned			pc;
extern unsigned 		top;
extern avm_memcell retval;


void execute_newtable (instruction* ); 
void execute_tablegetelem (instruction*);
void execute_tablesetelem (instruction*);


#endif
