#ifndef EQ_IM_H_
#define EQ_IM_H_

extern avm_memcell ax, bx, cx;
extern unsigned char	executionFinished;
extern unsigned			pc;

typedef unsigned char (*tobool_func_t)(avm_memcell*);
//typedef bool (*cmp_func) (double, double);

unsigned char number_tobool (avm_memcell* );
unsigned char string_tobool (avm_memcell* );
unsigned char bool_tobool (avm_memcell* );
unsigned char table_tobool (avm_memcell* );
unsigned char userfunc_tobool (avm_memcell* );
unsigned char libfunc_tobool (avm_memcell* );
unsigned char nil_tobool (avm_memcell* );
unsigned char undef_tobool (avm_memcell* );


// Tables 
static tobool_func_t toboolFuncs[] = {
	number_tobool, 
	string_tobool, 
	bool_tobool,
	table_tobool,
	userfunc_tobool,
	libfunc_tobool,
	nil_tobool,
	undef_tobool
};

static char* typeStrings[] = {
	"number",
	"string",
	"table",
	"userfunc",
	"libfunc",
	"nil",
	"undef"
};



unsigned char avm_tobool (avm_memcell* );

void execute_jeq(instruction* );
void execute_jne(instruction* );
void execute_jge(instruction* );
void execute_jle(instruction* );
void execute_jgt(instruction* );
void execute_jlt(instruction* );

void execute_jump(instruction* );


#endif
