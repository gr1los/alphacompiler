#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "quads_lib.h"
#include "avm_lib.h"
#include "eq_im.h"
#include "func_im.h"
#include "table_im.h"


void execute_newtable (instruction* instr) {
	avm_memcell* lv = avm_translate_operand(&instr->result, (avm_memcell*) 0);
	//assert(lv && (&stack[AVM_STACKSIZE-1] >= lv && lv > &stack[top] || lv == &retval));
	
	avm_memcellclear(lv);
	
	lv->type = table_m;
	lv->data.tableVal = avm_tablenew();
	avm_tableincrefcounter (lv->data.tableVal);
}

void execute_tablegetelem (instruction* instr) {
	avm_memcell* lv = avm_translate_operand(&instr->result, (avm_memcell*) 0);
	avm_memcell* t  = avm_translate_operand(&instr->arg1, (avm_memcell*) 0);
	avm_memcell* i  = avm_translate_operand(&instr->arg2, &ax);
	
	//assert((lv) && (&stack[AVM_STACKSIZE-1] >= lv) && (lv > &stack[top]) || (lv==&retval));
	//assert(t  && (&stack[AVM_STACKSIZE-1] >= t ) &&  (t > &stack[top] ));
	assert(i);
	
	avm_memcellclear(lv);
	lv->type = nil_m;
	
	if(t->type != table_m){
		fprintf(stderr,"illegal use of type %s as table! \n", typeStrings[t->type]);
		executionFinished = 1;
	}else{
		avm_memcell* content = avm_tablegetelem(t->data.tableVal, i);
		if (content) {
			avm_assign(lv, content);
		}else{	
			char* ts = avm_tostring(t);
			char* is = avm_tostring(i);
			fprintf(stderr,"%s[%s] not found!\n" , ts, is);
			executionFinished = 1;
			free(ts);
			free(is);
		}
	}
}

void execute_tablesetelem (instruction* instr) {
	avm_memcell* t = avm_translate_operand(&instr->result, (avm_memcell*) 0);
	avm_memcell* i = avm_translate_operand(&instr->arg1, &ax);
	avm_memcell* c = avm_translate_operand(&instr->arg2, &bx);
	
	//assert(t && (&stack[AVM_STACKSIZE-1] >= t) && (t> &stack[top]));
	assert (i && c);
	
	if(t->type != table_m){
		fprintf(stderr,"illegal use of type %s as table!\n", typeStrings[t->type]);
		executionFinished = 1;
	}	
	else
		avm_tablesetelem(t->data.tableVal, i, c);
}
