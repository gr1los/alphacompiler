/*
 * hash_table.c
 */

#include "hash_table.h"

unsigned int	tmpIndex = 0;		// temp var index
unsigned int programVarOffset = 0;
unsigned int functionLocalOffset = 0;
unsigned int formalArgOffset = 0;

SymbolTableEntry* createSTEntry (char* name, int scope, int line, SymbolTableType type) {
	SymbolTableEntry* newSTEntry;
	
	newSTEntry = (SymbolTableEntry*)malloc(sizeof(SymbolTableEntry));
	
	if ( newSTEntry == NULL ) {
		return NULL;
	}
	if (type == GLOBAL_V || type == LOCAL_V || type == FORMAL_V) {
		newSTEntry -> value.varVal = (Variable*)malloc(sizeof(Variable));
		newSTEntry -> value.varVal -> name = strdup(name);
		newSTEntry -> value.varVal -> scope = scope;
		newSTEntry -> value.varVal -> line = line;
	}else{
		newSTEntry -> value.funcVal = (Function*)malloc(sizeof(Function));
		newSTEntry -> value.funcVal -> name = strdup(name);
		newSTEntry -> value.funcVal -> scope = scope;
		newSTEntry -> value.funcVal -> line = line;
	}
	
	if (type == LOCAL_V && scope == 0) {
		newSTEntry -> type = GLOBAL_V;
	}else{
		newSTEntry -> type = type;
	}
	newSTEntry -> isActive = 1;
	
	return newSTEntry;
}

int isVariable (SymbolTableEntry *stEntry) {
	if (stEntry->type == GLOBAL_V 
		|| stEntry->type == LOCAL_V 
		|| stEntry->type == FORMAL_V){
			
		return 1;
	}
	return 0;
}

char* getSTEName(SymbolTableEntry* stEntry) {
	if ( isVariable(stEntry) ){
		return stEntry -> value.varVal -> name;
	}else{
		return stEntry -> value.funcVal -> name;
	}
}

int getSTEScope(SymbolTableEntry* stEntry) {
	if ( isVariable(stEntry) ){
		return stEntry -> value.varVal -> scope;
	}else{
		return stEntry -> value.funcVal -> scope;
	}
}

enum scopespace_e getSTESpace(SymbolTableEntry* stEntry){
	if ( isVariable(stEntry) ){
		return stEntry -> value.varVal -> space;
	}else{
		return stEntry -> value.funcVal -> space;
	}
}

unsigned int getSTEOffset(SymbolTableEntry* stEntry){
	if ( isVariable(stEntry) ){
		return stEntry -> value.varVal -> offset;
	}else{
		return stEntry -> value.funcVal -> offset;
	}
}

unsigned int getSTElocals(SymbolTableEntry* stEntry){
	if ( isVariable(stEntry) ){
		return 0;
	}else{
		return stEntry -> value.funcVal -> locals;
	}
}

unsigned int getSTEiaddress(SymbolTableEntry* stEntry){
	if ( isVariable(stEntry) ){
		return 0;
	}else{
		return stEntry -> value.funcVal -> iaddress;
	}
}


/* lose lose hashing */
int strHash(const char *str) {
	int hash = 0;
	int c;
	
	while (c = *str++)
		hash += c;

	return hash;
}

static void initHashTable (hTable *hashTable) {
	unsigned int libFuncSize = 12; // update it always
	char *libFunc[] = {
		"print", "input", "objectmemberkeys",
		"objecttotalmembers", "objectcopy", "totalarguments",
		"argument", "typeof", "strtonum", "sqrt", "cos", "sin" };
	unsigned int i;
	for (i=0 ;i<libFuncSize; i++) {
		addToHashTable(hashTable, createSTEntry(libFunc[i],0,0,LIBFUNC));
	}
	return;
}

int createHTable (hTable **hashTable) {
	hTable *tmpHTable;
	hBucket **hList;
	
	// error on malloc
	if ( !(tmpHTable = (hTable*)malloc(sizeof(hTable))) ){
		*hashTable = NULL;
		return 0;
	}
	
	if ( !(hList = (hBucket**)malloc(sizeof(hBucket*) * HASH_SIZE)) ){
		free(tmpHTable);
		*hashTable = NULL;
		return 0;
	}
	
	// everything it's ok !
	tmpHTable->size = HASH_SIZE;
	tmpHTable->buckets = hList;
	
	*hashTable = tmpHTable;
	
	initHashTable(*hashTable);
	return 1;
}

int destroyHTable (hTable **hashTable) {
	hTable *tmpHTable;
	hBucket *tmpList, *list;
	int i;
	
	tmpHTable = *hashTable;
	if (tmpHTable == NULL) {
		return 1;
	}
	
	for (i=0; i<tmpHTable->size; i++) {
		list = *(tmpHTable->buckets + i);
		while (list) {
			tmpList = list;
			list = list->next;
			free(tmpList);
		}
	}
	free(tmpHTable->buckets);
	free(tmpHTable);
	*hashTable = NULL;
	
	return 1;
}

int addToHashTable (hTable *hashTable, SymbolTableEntry *stEntry ) {
	int hashCode, index;
	hBucket *bList, *prev;
	
	// generate hash code
	hashCode = strHash(getSTEName(stEntry));
	
	// find the the hash bucket
	index = hashCode % (hashTable->size);
	bList = *(hashTable->buckets + index);
	
	if (!bList) {		// list is empty, create it
		if ( !(bList = (hBucket*)malloc(sizeof(hBucket))) ) {
			return 0; // failed malloc
		}
		*(hashTable->buckets + index) = bList;
		
		bList->stEntry = stEntry;
		bList->next = NULL;
		
		return 1;
	}
	
	prev = bList;
	while (bList) {		// traverse list
		prev = bList;
		bList = bList -> next;
	}
	
	if ( !(bList = (hBucket*)malloc(sizeof(hBucket))) ) {
		return 0;
	}
	bList -> stEntry = stEntry;
	bList -> next = NULL;
	prev -> next = bList;
	
	return 1;
}

SymbolTableEntry* searchHashTable(hTable *hashTable, char *name, int scope) {
	int hashCode, index;
	hBucket *bList, *prev;
	
	// generate hash code
	hashCode = strHash(name);
	
	// find the the hash bucket
	index = hashCode % (hashTable->size);
	bList = *(hashTable->buckets + index);
	
	
	while (bList) {
		if ( !strcmp(name, getSTEName(bList->stEntry)) && bList->stEntry->isActive) {
			if ( scope < 0 ) {
				return bList->stEntry;
			}else{
				if ( isVariable(bList->stEntry)) {
					if ( bList->stEntry->value.varVal->scope == scope) {
						return bList->stEntry;
					}
				}else{
					if ( bList->stEntry->value.funcVal->scope == scope) {
						return bList->stEntry;
					}
				}
			}
		}
		bList = bList -> next;
	}
	
	return NULL;
}

SymbolTableEntry* newtemp(hTable *hashTable){
	SymbolTableEntry *tmpSTE, *found;
	char tmpName[100];
	
	sprintf(tmpName,"_t%d",tmpIndex++);
	found = searchHashTable(hashTable, tmpName, -1);
	if (found == NULL) {
		tmpSTE = createSTEntry(tmpName,0,1,LOCAL_V);
		addToHashTable(symTable,tmpSTE);
		return tmpSTE;
	}
	return found;
}

int istempname(char* name){
	return (name[0] == '_');
}

void resettemp(void) {
	tmpIndex = 0;
}

void hideScope (hTable *hashTable, int scope) {
	hBucket *list;
	int i;
	
	if ( !hashTable ){
		return;
	}
	
	for (i=0; i<hashTable->size; i++ ){
		list = *(hashTable->buckets + i);
		while (list) {
			if ( list -> stEntry -> isActive && list -> stEntry -> type != LIBFUNC) {
				if ( list->stEntry->value.varVal->scope == scope
					|| list->stEntry->value.funcVal-> scope == scope){
					list -> stEntry -> isActive = 0;
				}
			}
			list = list -> next;
		}
	}
}

int printHashTable (hTable *hashTable) {
	const char *enumStr[] = {"GLOBAL_V", "LOCAL_V", 
			"FORMAL_V", "USERFUNC", "LIBFUNC"};
	hBucket *list;
	int i;
	
	if ( !hashTable ){
		return 0;
	}
	
	for (i=0; i<hashTable->size; i++ ){
		list = *(hashTable->buckets + i);
		while (list) {
			printf("%s: ",enumStr[list->stEntry->type]);
			if ( isVariable(list->stEntry) ) {
				printf("%s Scope=%d Line=%d Offset=%d Active=%d\n",
						list->stEntry->value.varVal->name,
						list->stEntry->value.varVal->scope,
						list->stEntry->value.varVal->line,
						list->stEntry->value.varVal->offset,
						list->stEntry->isActive);
			}else{
				printf("%s Scope=%d Line=%d Offset=%d Active=%d\n",
						list->stEntry->value.funcVal->name,
						list->stEntry->value.funcVal->scope,
						list->stEntry->value.funcVal->line,
						list->stEntry->value.funcVal->offset,
						list->stEntry->isActive);
			}
			list = list -> next;
		}
	}
	
}

void setSpace(SymbolTableEntry* stEntry, scopespace_e space){
	if ( isVariable(stEntry) ){
		stEntry -> value.varVal -> space = space;
	}else{
		stEntry -> value.funcVal -> space = space;
	}
}

void setOffset(SymbolTableEntry* stEntry, unsigned int offset){
	if ( isVariable(stEntry) ){
		stEntry -> value.varVal -> offset = offset;
	}else{
		stEntry -> value.funcVal -> offset = offset;
	}
}

void setAddress(SymbolTableEntry* stEntry, unsigned int iaddress){
	stEntry -> value.funcVal -> iaddress = iaddress;
}

void setLocals(SymbolTableEntry* stEntry, unsigned int locals){
	stEntry -> value.funcVal -> locals = locals;
}

scopespace_e currscopespace(void) {
	if (scopeSpaceCounter == 1) {
		return programvar;
	}else if (scopeSpaceCounter % 2 == 0) {
		return formalarg;
	}else{
		return functionlocal;
	}
}

unsigned int currscopeoffset(void) {
	switch (currscopespace()) {
		case programvar		: return programVarOffset;
		case functionlocal	: return functionLocalOffset;
		case formalarg		: return formalArgOffset;
		default: {printf("currscopeoffset\n"); return 0; }
	}
}

void inccurrscopeoffset(void) {
	switch (currscopespace()) {
		case programvar		: {++programVarOffset; break;}
		case functionlocal	: {++functionLocalOffset; break;}
		case formalarg		: {++formalArgOffset; break;}
		default: {printf("inccurrscopeoffset\n"); return; }
	}
}

void resetformalargsoffset(void){
	formalArgOffset = 0;
}

void restorecurrscopeoffset(unsigned n){
	switch(currscopespace()){
		case programvar		: { programVarOffset = n; break; }
		case functionlocal	: { functionLocalOffset = n; break; }
		case formalarg	  	: { formalArgOffset = n; break; }
		default				: { printf("restorecurrscopeoffset\n"); return; }
	}
}

void resetfunctionlocaloffset(void){
	functionLocalOffset = 0;
}


/*
int main(int argc, char **argv) {
	hTable *hashTable;
	createHTable(&hashTable);
	addToHashTable(hashTable,createSTEntry("test",0,1,GLOBAL_V));
	addToHashTable(hashTable,createSTEntry("test1",1,2,LOCAL_V));
	addToHashTable(hashTable,createSTEntry("test1",1,3,USERFUNC));
	addToHashTable(hashTable,createSTEntry("test3",3,4,LIBFUNC));
	addToHashTable(hashTable,createSTEntry("test4",4,5,FORMAL_V));
	printHashTable(hashTable);
	
	printf("Search for test1 everywhere\n");
	if (searchHashTable(hashTable,"test1",-1)) {
		printf("found\n");
	}else{
		printf("Not found\n");
	}
	printf("Search for test1 at scope = 0\n");
	if (searchHashTable(hashTable,"test1",0)) {
		printf("found\n");
	}else{
		printf("Not found\n");
	}
	hideScope(hashTable,1);
	printHashTable(hashTable);

	return 0;
}
*/

