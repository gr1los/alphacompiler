%{
	#include <stdio.h>
	#include <string.h>
	#include "hash_table.h"
	#include "quads_lib.h"

	int yyerror (char* yaccProvideMessage);
	int yylex (void);
	
	hTable *symTable;
	
	stack_s functionLocalsStack = {
		.len = 0
	};
	
	stack_s loopcounterstack = {
		.len = 0
	};
	
	unsigned int globalScope;
	unsigned int scopeSpaceCounter = 1;
	
	int functionIndex = 0;
	int functionDef = 0;
	int formalDef = 0;
	int foundLoop = 0;
	int loopcounter = 0;
	
	int breakInit = 0;
	int contInit = 0;
	
	int foundError = 0;
	
	break_s *bList = NULL;
	continue_s *cList = NULL;
	
	list_s* trueList = NULL;
	list_s* falseList = NULL;
	
	extern int yylineno;
	extern char* yytext;
	extern FILE* yyin;
	
	
%}
%union
{
	char*			strValue;
	int				intValue;
	double			floatValue;
	expr_s*			exprValue;
	enum iopcode	opValue;
	forstmt_s		forValue;
	struct SymbolTableEntry_t* entryValue;
	callsuffix_s	callValue;
	indexelem_s		indexValue;
	stmt_s			stmtValue;
}
%{
	#define YYPRINT(File, Type, Value) token_value (File, Type, &Value)
	static void token_value (FILE *file, int type, const YYSTYPE *value);
%}
%debug

%token END 0
%token <strValue>	IF ELSE WHILE FOR FUNCTION RETURN BREAK CONTINUE AND 
%token <strValue>	NOT OR LOCAL TRUE FALSE NIL

%token <strValue>	EQUAL GREAT_EQUAL LESS_EQUAL ASSIGN GREAT_THAN LESS_THAN
%token <strValue>	PLUS PLUS_1 MINUS MINUS_1 MUL DIV MOD UNEQUAL

%token <strValue>	L_BRACK R_BRACK L_HOOK R_HOOK L_PARENTH R_PARENTH SEMICOLON
%token <strValue>	COMMA COLON DBL_COL DOT DOTS

%token <strValue>	ID STRING
%token <intValue>	INTEGER
%token <floatValue>	FLOAT

%right ASSIGN
%left OR
%left AND
%nonassoc EQUAL UNEQUAL
%nonassoc GREAT_THAN GREAT_EQUAL LESS_THAN LESS_EQUAL
%left PLUS MINUS
%left MUL DIV MOD
%right NOT PLUS_1 MINUS_1 UMINUS
%left DOT DOTS
%left L_BRACK R_BRACK 
%left L_PARENTH R_PARENTH

%type <strValue>	ifstmt whilestmt forstmt returnstmt
%type <strValue>	funcname funcblockstart funcblockend
%type <strValue>	loopstart loopend
%type <exprValue>	lvalue expr const assignexpr term member primary
%type <exprValue>	elist call objectdef
%type <intValue>	ifprefix elseprefix N M funcargs funcbody
%type <opValue>		arithop relop whilestart whilecond boolop
%type <forValue>	forprefix
%type <entryValue>	funprefix funcdef
%type <callValue>	callsuffix normcall methodcall
%type <indexValue>	indexed indexedelem
%type <stmtValue>	stmt loopstmt block_stmt block


%start program
%expect 1

%%

program:		stmt { resettemp(); } program	{globalScope = 0;}
				| /* empty */					{}
				;

stmt:			expr SEMICOLON					{printf("expr;\n");}
				| ifstmt						{printf("ifstmt\n");}
				| whilestmt						{printf("while\n");}
				| forstmt						{printf("forstmt\n");}
				| returnstmt
				{	if (!functionDef) {
						fprintf(stderr,"%d: ERROR found return outside function.\n",yylineno);
						foundError++;
					}
					printf("returnstmt\n");
				}
				| BREAK SEMICOLON
				{	printf("break;\n");
					if (!foundLoop) {
						fprintf(stderr,"%d: ERROR found break outside loop.\n",yylineno);
						foundError++;
					}
					if (breakInit == 0){
						$$.breakList = NULL;
						breakInit = 1;
					}
					
					bList = newlist_b(nextquad(), bList);
					emit(jump, NULL, NULL, NULL);
				}
				| CONTINUE SEMICOLON
				{	printf("continue;\n");
					if (!foundLoop) {
						fprintf(stderr,"%d: ERROR found continue outside loop.\n",yylineno);
						foundError++;
					}
					if (contInit == 0){
						$$.continueList = NULL;
						contInit = 1;
					}
					
					cList = newlist_c(nextquad(), cList);
					emit(jump, NULL, NULL, NULL);
				}
				| block							{printf("block\n"); memcpy(&$$, &$1, sizeof(stmt_s));}
				| funcdef 						{printf("funcdef\n");}
				| SEMICOLON						{printf("semicolon\n");}
				;

expr:			assignexpr
				{	printf("assignexpr\n");
				}
				| expr arithop expr
				{	$$ = newexpr(arithexpr_e);
					$$->sym = newtemp(symTable);
					emit($2, $$, $1, $3);
				} %prec MUL
				| expr relop expr
				{	
					expr_s* tmpBool1 = newexpr(constbool_e);
					expr_s* tmpBool2 = newexpr(constbool_e);
					$$ = newexpr(boolexpr_e);
					$$->sym = newtemp(symTable);
					
					emit($2, $1, $3, NULL);
					patchlabel(currquad(), nextquad() + 3);
					tmpBool1->boolConst = 0;
					emit(assign, $$, NULL, tmpBool1);
					emit(jump, NULL, NULL, NULL);
					patchlabel(currquad(), nextquad() + 2);
					tmpBool2->boolConst = 1;
					emit(assign, $$, NULL, tmpBool2);
					
					/*
					trueList = newlist(nextquad(), trueList);
					falseList = newlist(nextquad() + 1, falseList);
					emit($2, $1, $3, NULL);
					emit(jump, NULL, NULL, NULL);
					*/
				} %prec GREAT_THAN
				| expr boolop expr
				{	$$ = newexpr(boolexpr_e);
					$$ -> sym = newtemp(symTable);
					emit($2, $$, $1, $3);
				} %prec AND
				| term
				{	printf("term\n");
				}
				;
				
arithop:		PLUS		{$$ = add;}
				| MINUS		{$$ = sub;}
				| MUL		{$$ = mul;}
				| DIV		{$$ = divide;}
				| MOD		{$$ = mod;}
				;
				
relop:			GREAT_THAN		{$$ = if_greater;}
				| GREAT_EQUAL	{$$ = if_greateq;}
				| LESS_THAN		{$$ = if_less;}
				| LESS_EQUAL	{$$ = if_lesseq;}
				| EQUAL			{$$ = if_eq;}
				| UNEQUAL		{$$ = if_noteq;}
				;
				
boolop:			AND				{$$ = and;}
				| OR			{$$ = or;}
				;

term:			L_PARENTH expr R_PARENTH
				{	printf("(expr)\n");
					$$ = $2;
				}
				| MINUS expr %prec UMINUS
				{	printf("-expr\n");
					if (!checkuminus($2)){
						fprintf(stderr,"%d: ERROR Illegal expr to unary\n",yylineno);
						foundError++;
					}
					$$ = newexpr(arithexpr_e);
					$$->sym = newtemp(symTable);
					emit(uminus, $$, $2, NULL);
				}
				| NOT expr
				{	printf("!expr\n");
					
					$$ = newexpr(boolexpr_e);
					$$->sym = newtemp(symTable);
					emit(not, $$, $2, NULL);
					
					/*
					list_s *tmp = trueList;
					trueList = falseList;
					falseList = tmp;
					*/
				}
				| PLUS_1 lvalue
				{	printf("++lvalue\n");
					SymbolTableEntry *tmpEntry;
					tmpEntry = searchHashTable(symTable,getSTEName($2->sym),globalScope);
					if ( tmpEntry != NULL ) {
						if ( tmpEntry -> type == USERFUNC 
							|| tmpEntry -> type == LIBFUNC) {
							fprintf(stderr,"%d: ERROR cannot change value of function : %s\n",yylineno,getSTEName($2->sym));
							foundError++;
							}
					}
					expr_s *tmpConst;
					
					// init const expr
					tmpConst = newexpr(constnum_e);
					tmpConst -> numConst = 1;
					
					if ($2 -> type == tableitem_e) {
						$$ = emit_iftableitem($2);
						emit(add, $$, $$, tmpConst);
						emit(tablesetelem, $2, $2->index, $$);
					}else{
						emit(add, $2, $2, tmpConst);
						$$ = newexpr(arithexpr_e);
						$$ -> sym = newtemp(symTable);
						emit(assign, $$, $2, NULL);
					}
				}
				| lvalue PLUS_1
				{	printf("lvalue++\n");
					SymbolTableEntry *tmpEntry;
					tmpEntry = searchHashTable(symTable,getSTEName($1->sym),globalScope);
					if ( tmpEntry != NULL ) {
						if ( tmpEntry -> type == USERFUNC 
							|| tmpEntry -> type == LIBFUNC) {
							fprintf(stderr,"%d: ERROR cannot change value of function : %s\n",yylineno,getSTEName($1->sym));
							foundError++;
							}
					}
					expr_s *value, *tmpConst;
					$$ = newexpr(var_e);
					$$ -> sym = newtemp(symTable);
					
					// init const expr
					tmpConst = newexpr(constnum_e);
					tmpConst -> numConst = 1;
					
					if ($1->type == tableitem_e) {
						value = emit_iftableitem($1);
						emit(assign, $$, value, NULL);
						emit(add, value, value, tmpConst);
						emit(tablesetelem, $1, $1->index, value);
					}else{
						emit(assign, $$, $1, NULL);
						emit(add, $1, $1, tmpConst);
					}
				}
				| MINUS_1 lvalue
				{	printf("--lvalue\n");
					SymbolTableEntry *tmpEntry;
					tmpEntry = searchHashTable(symTable,getSTEName($2->sym),globalScope);
					if ( tmpEntry != NULL ) {
						if ( tmpEntry -> type == USERFUNC 
							|| tmpEntry -> type == LIBFUNC) {
							fprintf(stderr,"%d: ERROR cannot change value of function : %s\n",yylineno,getSTEName($2->sym));
							foundError++;
							}
					}
					expr_s *tmpConst;
					
					// init const expr
					tmpConst = newexpr(constnum_e);
					tmpConst -> numConst = 1;
					
					if ($2 -> type == tableitem_e) {
						$$ = emit_iftableitem($2);
						emit(sub, $$, $$, tmpConst);
						emit(tablesetelem, $2, $2->index, $$);
					}else{
						emit(sub, $2, $2, tmpConst);
						$$ = newexpr(arithexpr_e);
						$$ -> sym = newtemp(symTable);
						emit(assign, $$, $2, NULL);
					}
				}
				| lvalue MINUS_1
				{	printf("lvalue--\n");
					SymbolTableEntry *tmpEntry;
					tmpEntry = searchHashTable(symTable,getSTEName($1->sym),globalScope);
					if ( tmpEntry != NULL ) {
						if ( tmpEntry -> type == USERFUNC 
							|| tmpEntry -> type == LIBFUNC) {
							fprintf(stderr,"%d: ERROR cannot change value of function : %s\n",yylineno,getSTEName($1->sym));
							foundError++;
							}
					}
					expr_s *value, *tmpConst;
					$$ = newexpr(var_e);
					$$ -> sym = newtemp(symTable);
					
					// init const expr
					tmpConst = newexpr(constnum_e);
					tmpConst -> numConst = 1;
					
					if ($1->type == tableitem_e) {
						value = emit_iftableitem($1);
						emit(assign, $$, value, NULL);
						emit(sub, value, value, tmpConst);
						emit(tablesetelem, $1, $1->index, value);
					}else{
						emit(assign, $$, $1, NULL);
						emit(sub, $1, $1, tmpConst);
					}
				}
				| primary						{printf("primary\n");}
				;
				
assignexpr:		lvalue ASSIGN expr
				{	printf("lvalue = expr\n");
					/*
					SymbolTableEntry *tmpEntry;
					tmpEntry = searchHashTable(symTable,getSTEName($1->sym),globalScope);
					if ( tmpEntry != NULL ) {
						if ( tmpEntry -> type == USERFUNC 
							|| tmpEntry -> type == LIBFUNC) {
							fprintf(stderr,"%d: ERROR cannot change value of function : %s\n",yylineno,getSTEName($1->sym));
							}
					}
					* better solution in else 
					*/
					
					// check if lvalue is table item
					if ($1->type == tableitem_e){
						emit(tablesetelem, $1, $1->index, $3);
						$$ = emit_iftableitem($1);
						$$->type = assignexpr_e;
					}else{
						/*
						emit(assign, $1, NULL, $3);
						// den doulevei panta
						$$ = newexpr(assignexpr_e);
						$$->sym = newtemp();
						emit(assign, $$, NULL, $1);
						*/
						// search for formal argument in different scope
						if ( $1 -> sym -> type == USERFUNC || $1 -> sym -> type == LIBFUNC) {
							fprintf(stderr,"%d: ERROR cannot change value of function : %s\n",yylineno,getSTEName($1->sym));
							foundError++;
						}
						emit(assign, $1, NULL, $3);
						$$ = newexpr(assignexpr_e);
						$$ -> sym = newtemp(symTable);
						emit(assign, $$, NULL, $1);
					}
				}
				;
				
primary:		lvalue
				{	printf("lvalue\n");
					$$ = emit_iftableitem($1);
				}
				| call							{printf("call\n");}
				| objectdef						{printf("objectdef\n");}
				| L_PARENTH funcdef R_PARENTH
				{	printf("( funcdef )\n");
					$$ = newexpr(programfunc_e);
					$$->sym = $2;
				}
				| const							{printf("const\n");}
				;

lvalue:			ID
				{	printf("id\n");
					//$$ = $1;
					SymbolTableEntry *scopeEntry, *symbol, *globalEntry;
					/*
					int i;
					for (i=globalScope; i>=0; i--){
						scopeEntry = searchHashTable(symTable,$1,i);
						if (scopeEntry == NULL) {
							symbol = createSTEntry($1,i,yylineno,LOCAL_V);
							setSpace(symbol, currscopespace());
							setOffset(symbol, currscopeoffset());
							inccurrscopeoffset();
							addToHashTable(symTable,symbol);
							$$ = lvalue_expr(symbol);
							break;
						}else{
							if ( functionDef==1 && (scopeEntry->type==LOCAL_V || scopeEntry->type==FORMAL_V)&& getSTEScope(scopeEntry)!=i ){
								fprintf(stderr,"%d: cannot access %s in scope %d\n",yylineno,getSTEName(scopeEntry),i);
								break;
							}else{
								// formal fix here
								//printf("globalScope:%d\n",globalScope);
								//scopeEntry = searchHashTable(symTable, $1, globalScope);
								$$ = lvalue_expr(scopeEntry);
								break;
							}
						}
					}
					*/
					int i, found = 0;
					for (i=globalScope; i>=0; i--){
						scopeEntry = searchHashTable(symTable,$1,i);
						if (scopeEntry != NULL){
							found = 1;
							break;
						}
					}
					if (found == 0) {
						
						symbol = createSTEntry($1,globalScope,yylineno,LOCAL_V);
						
						setSpace(symbol, currscopespace());
						setOffset(symbol, currscopeoffset());
						inccurrscopeoffset();
						addToHashTable(symTable,symbol);
						$$ = lvalue_expr(symbol);
					}else if (found == 1 && functionDef == 1){
						globalEntry = searchHashTable(symTable,$1,0);
						if ( globalEntry != NULL && globalEntry->type == GLOBAL_V && (strcmp(getSTEName(globalEntry),$1) == 0)){
							
							fprintf(stderr,"%d: %s Refers to global\n",yylineno,$1);
							$$ = lvalue_expr(globalEntry);
						}else{
							if (getSTEScope(scopeEntry)!=globalScope) {
								fprintf(stderr,"%d: ERROR cannot access %s.\n",yylineno,$1);
								foundError++;
							}else{
								//fprintf(stderr,"%d: cannot access %s in scope %d\n",yylineno,getSTEName(globalEntry),globalScope);
								symbol = createSTEntry($1,globalScope,yylineno,LOCAL_V);
								setSpace(symbol, currscopespace());
								setOffset(symbol, currscopeoffset());
								inccurrscopeoffset();
								addToHashTable(symTable,symbol);
								$$ = lvalue_expr(symbol);
							}
							
						}
					}else{
						$$ = lvalue_expr(scopeEntry);
					}
				}
				| LOCAL ID
				{	printf("local id\n");
					//$$ = $2;
					SymbolTableEntry *scopeEntry;
					SymbolTableEntry *globalEntry, *symbol;
					/*
					int i;
					for (i=globalScope; i>=0; i--){
						scopeEntry = searchHashTable(symTable,$2,i);
						if( scopeEntry != NULL){
							fprintf(stderr,"%d: Found locally %s.\n",yylineno,$2);
							$$ = lvalue_expr(scopeEntry);
							break;
						}else{
							globalEntry = searchHashTable(symTable,$2,0);
							if( globalEntry != NULL && globalEntry->type == LIBFUNC){
								fprintf(stderr,"%d: ERROR Illegal usage of variable %s.\n",yylineno,$2);
								foundError++;
								break;
							}else{
								symbol = createSTEntry($2,i,yylineno,LOCAL_V);
								setSpace(symbol, currscopespace());
								setOffset(symbol, currscopeoffset());
								inccurrscopeoffset();
								addToHashTable(symTable, symbol);
								$$ = lvalue_expr(symbol);
								fprintf(stderr,"%d: Added local variable %s.\n",yylineno,$2);
								break;
							}
						}
					}
					*/
					scopeEntry = searchHashTable(symTable,$2,globalScope);
					if (scopeEntry != NULL) {
						if (scopeEntry->type == USERFUNC || scopeEntry->type == LOCAL_V) {
							fprintf(stderr,"%d: Found locally %s.\n",yylineno,$2);
							$$ = lvalue_expr(scopeEntry);
						}
					}else{
						if (searchHashTable(symTable,$2,0)->type != LIBFUNC){	// check for collision with libfuncs
							symbol = createSTEntry($2,globalScope,yylineno,LOCAL_V);
							setSpace(symbol, currscopespace());
							setOffset(symbol, currscopeoffset());
							inccurrscopeoffset();
							addToHashTable(symTable, symbol);
							$$ = lvalue_expr(symbol);
							fprintf(stderr,"%d: Added local variable %s.\n",yylineno,$2);
						}else{
							fprintf(stderr,"%d: ERROR Illegal usage of variable %s.\n",yylineno,$2);
							foundError++;
						}
					}
				}
				| DBL_COL ID
				{	printf("::id\n");
					//$$ = $2;
					SymbolTableEntry *globalEntry = searchHashTable(symTable,$2,0);
					if( globalEntry != NULL){
						fprintf(stderr,"%d: Found globally %s.\n",yylineno,$2);
						$$ = lvalue_expr(globalEntry);
					}else{
						fprintf(stderr,"%d: ERROR Illegal usage of variable %s.\n",yylineno,$2);
						foundError++;
					}
				 }
				| member						{printf("member\n");}
				;

member:			lvalue DOT ID
				{	printf("lvalue.id\n");
					$$ = member_item($1, $3);
				}
				| lvalue L_BRACK expr R_BRACK
				{	printf("lvalue[expr]\n");
					$1 = emit_iftableitem($1);
					$$ = newexpr(tableitem_e);
					$$->sym = $1->sym;
					$$->index = $3;
				}
				| call DOT ID
				{	printf("call.id\n");
					$$ = member_item($1, $3);
				}
				| call L_BRACK expr R_BRACK		{printf("call[expr]\n");}
				;

call:			call L_PARENTH elist R_PARENTH
				{	printf("call(elist)\n");
					$$ = make_call($1, $3);
					$3 = NULL; // TODO: free
				}
				| lvalue callsuffix
				{	printf("lvalue callsuffix\n");
					if ($2.method) {
						expr_s* self = $1;
						$1 = emit_iftableitem(member_item(self, $2.name));
						self -> next = $2.elist;	// add to front
						$2.elist = self;
					}
					$$ = make_call($1, $2.elist);
					$2.elist = NULL; // TODO: free
				}
				| L_PARENTH funcdef R_PARENTH L_PARENTH elist R_PARENTH	
				{	printf("(funcdef)(elist)\n");
					expr_s* func = newexpr(programfunc_e);
					func -> sym = $2;
					$$ = make_call(func, $5);
					$5 = NULL; // TODO: free
				}
				;
				
callsuffix:		normcall			{printf("normcall\n"); $$ = $1;}
				| methodcall		{printf("methodcall\n"); $$ = $1;}
				;
				
normcall:		L_PARENTH elist R_PARENTH
				{	printf("(elist)\n");
					$$.elist = $2;
					$$.method = 0;
					$$.name = NULL;
				}
				;
				
methodcall:		DOTS ID L_PARENTH elist R_PARENTH
				{	printf("..id(elist)\n");
					$$.elist = $4;
					$$.method = 1;
					$$.name = $2;
				}
				;

elist:			expr								{printf("expr\n"); $$ = $1;}
				| elist COMMA expr
				{	expr_s* curr = $1;
					printf("elist,expr\n");
					while(curr->next != NULL){
						curr = curr->next;
					}
					curr->next = $3;
					$3->next = NULL;
				}
				| /* empty */						{;}
				;

objectdef:		L_BRACK elist R_BRACK
				{	expr_s* table = newexpr(newtable_e);
					expr_s* curr = $2;
					expr_s* tmpExpr;
					int i = 0;
					
					printf("[elist]\n");
					table->sym = newtemp(symTable);
					emit(tablecreate, table, NULL, NULL);
					
					while (curr != NULL) {
						tmpExpr = newexpr(constnum_e);
						tmpExpr -> numConst = i++;
						emit(tablesetelem, table, tmpExpr, curr);
						curr = curr -> next;
					}
					$2 = NULL; // TODO: free
					$$ = table;
				}
				| L_BRACK indexed R_BRACK
				{	printf("[indexed]\n");
					expr_s* table = newexpr(newtable_e);
					expr_s *r1, *r2;
					table -> sym = newtemp(symTable);
					emit(tablecreate, table, NULL, NULL);
					
					r1 = $2.first;
					r2 = $2.second;
					
					while ( r1 != NULL || r2 != NULL) {
						emit(tablesetelem, table, r1, r2);
						r1 = r1 -> next;
						r2 = r2 -> next;
					}
					$$ = table;
					
					$2.first = NULL;	// TODO: free
					$2.second = NULL;	// TODO: free
				}
				;

indexed:		indexedelem
				{	printf("indexedelem\n");
					$$ = $1;
				}
				| indexed COMMA indexedelem
				{	printf("indexed,indexedelem\n");
					expr_s* curr;
					
					// first list
					curr = $1.first;
					while (curr -> next != NULL) {
						curr = curr -> next;
					}
					curr -> next = $3.first;
					$3.first -> next = NULL;
					
					// second list
					curr = $1.second;
					while (curr -> next != NULL) {
						curr = curr -> next;
					}
					curr -> next = $3.second;
					$3.second -> next = NULL;
				}
				;

indexedelem:	L_HOOK expr COLON expr R_HOOK
				{	printf("{expr:expr}\n");
					$$.first = $2;
					$$.second = $4;
				}

block_stmt:		block_stmt stmt
				{	printf("block_stmt stmt\n");
					//$$.breakList = merge_b($1.breakList, $2.breakList);
					//$$.continueList = merge_c($1.continueList, $2.continueList);
					
				}
				| /* empty */						{;}
				;

block:			L_HOOK {globalScope++;} block_stmt R_HOOK
				{	printf("{block_stmt}\n");
					hideScope(symTable,globalScope--);
					memcpy(&$$, &$3, sizeof(stmt_s));
				}
				;
				
				
funcblockstart: {	push(loopcounterstack,loopcounter); 
					loopcounter = 0;
				}
				;

funcblockend: 	{ 	loopcounter = pop(loopcounterstack); 
				}
				;

funcname:		ID
				{ 	printf("function id\n");
					$$ = $1;
				}	
				| /*empty*/
				{	printf("function (idlist) block\n");
					char tmpName[100];
					sprintf(tmpName,"_f%d",functionIndex++);
					$$ = tmpName;
				}
				;
				
funprefix:		FUNCTION funcname
				{	SymbolTableEntry* tmpEntry;
					SymbolTableEntry* newEntry;
					tmpEntry = searchHashTable(symTable,$2,globalScope);
					if ( tmpEntry != NULL){
						fprintf(stderr,"%d: ERROR %s already exists.\n",yylineno,$2);
						foundError++;
					}else{
						newEntry = createSTEntry($2,globalScope,yylineno,USERFUNC);
						addToHashTable(symTable,newEntry);
						$$ = newEntry;
						setAddress($$,nextquad()); //prwtos enoxos an den m douleuoun labels
						emit(funcstart,lvalue_expr($$),NULL,NULL);
						push(functionLocalsStack,functionLocalOffset); // save current offset
						scopeSpaceCounter++;
						resetformalargsoffset();
					}
				}
				;
		
funcargs:		L_PARENTH 
				{	globalScope++; 
					scopeSpaceCounter++;
					formalDef = 1;
				}
				idlist R_PARENTH
				{	formalDef = 0;
					globalScope--; 
					functionDef=1; 
					scopeSpaceCounter++;
					resetfunctionlocaloffset();
				}
				;
				
funcbody:		block
				{	functionDef=0;
					scopeSpaceCounter--;
					printf("function (idlist) block\n"); 
				}
				;
				
funcdef:		funprefix funcargs funcblockstart funcbody funcblockend
				{	scopeSpaceCounter--;
					setLocals($1,functionLocalOffset);
					functionLocalOffset = pop(functionLocalsStack); // Restore saved scope offset
					$$ = $1;
					emit(funcend,lvalue_expr($1),NULL,NULL);
				}
				;
				

const:			INTEGER
				{	printf("integer\n");
					$$ = newexpr(constnum_e);
					$$ -> numConst = $1;
				}
				| FLOAT
				{	printf("float\n");
					$$ = newexpr(constnum_e);
					$$ -> numConst = $1;
				}
				| STRING
				{	printf("string\n");
					$$ = newexpr(conststring_e);
					$$ -> strConst = $1;
				}
				| NIL
				{	printf("nil\n");
					$$ = newexpr(nil_e);
				}
				| TRUE
				{	printf("true\n");
					$$ = newexpr(constbool_e);
					$$ -> boolConst = 1;
				}
				| FALSE
				{	printf("false\n");
					$$ = newexpr(constbool_e);
					$$ -> boolConst = 0;
				}
				;
				
idlist:			ID 
				{	printf("id\n");
					SymbolTableEntry *globalEntry, *scopeEntry;
					scopeEntry = searchHashTable(symTable,$1,globalScope);
					globalEntry = searchHashTable(symTable,$1,-1);
					if ( scopeEntry != NULL || (globalEntry != NULL && globalEntry->type==LIBFUNC)) {
						fprintf(stderr,"%d: ERROR %s formal redeclaration.\n",yylineno,$1);
						foundError++;
					}else{
						addToHashTable(symTable,createSTEntry($1,globalScope,yylineno,FORMAL_V));
					}
				}
				| idlist COMMA ID
				{	printf("idlist,id\n");
					SymbolTableEntry *globalEntry, *scopeEntry;
					scopeEntry = searchHashTable(symTable,$3,globalScope);
					globalEntry = searchHashTable(symTable,$3,-1);
					if ( scopeEntry != NULL || (globalEntry != NULL && globalEntry->type==LIBFUNC)) {
						fprintf(stderr,"%d: ERROR %s formal redeclaration.\n",yylineno,$3);
						foundError++;
					}else{
						addToHashTable(symTable,createSTEntry($3,globalScope,yylineno,FORMAL_V));
					}
				}
				| /*empty*/										{}
				;

ifprefix:		IF L_PARENTH expr R_PARENTH
				{	expr_s* tmpBool = newexpr(constbool_e);
					tmpBool -> boolConst = 1;
					emit(if_eq, $3, tmpBool, NULL);
					patchlabel(currquad(),nextquad() + 2);
					$$ = nextquad();
					emit(jump, NULL, NULL, NULL);
				}
				;
				
elseprefix:		ELSE
				{	$$ = nextquad();
					emit(jump, NULL, NULL, NULL);
				}
				;

ifstmt:			ifprefix stmt
				{	printf("if (expr) stmt\n");
					patchlabel($1, nextquad() + 1); // lathos stis dialekseis old +0
				}
				| ifprefix stmt elseprefix stmt
				{	printf("if (expr) stmt else stmt\n");
					patchlabel($1, $3 + 2);			// lathos stis dialekseis old +1
					patchlabel($3, nextquad() + 1);
				}
				;

loopstart:		{	++loopcounter; }
				;

loopend:		{	--loopcounter; }
				;

loopstmt:		loopstart stmt loopend	{ memcpy(&$$, &$2, sizeof(stmt_s)); }
				;

whilestart:		WHILE
				{	foundLoop = 1;
					$$ = nextquad();
				}
				;
whilecond:		L_PARENTH expr R_PARENTH
				{	expr_s* tmpBool = newexpr(constbool_e);
					tmpBool->boolConst = 1;
					emit(if_eq, $2, tmpBool, NULL);
					patchlabel(currquad(), nextquad() + 2);
					$$ = nextquad();
					emit(jump, NULL, NULL, NULL);
				}
				;

whilestmt:		whilestart whilecond loopstmt
				{	foundLoop = 0; 
					printf("while (expr) stmt\n");
					emit(jump, NULL, NULL, NULL);
					patchlabel(currquad(), $1+1);
					
					patchlabel($2, nextquad() + 1);
					
					//break_s *bList = $3.breakList;
					//continue_s *cList = $3.continueList;
					break_s *list1 = bList;
					continue_s *list2 = cList;
					
					while (list1 != NULL) {
						patchlabel(list1->nextquad, nextquad() + 1);	//false jump
						list1 = list1 -> next;
					}
					
					while (list2 != NULL) {
						patchlabel(list2->nextquad, $1 + 1);		// closure jump
						list2 = list2 -> next;
					}
					
					// TODO: empty lists
					bList = NULL;
					cList = NULL;
					
				}
				;

N:				{	$$ = nextquad();
					emit(jump, NULL, NULL, NULL);
				}
				;
				
M:				{	$$ = nextquad(); }
				;

forprefix:		FOR L_PARENTH {foundLoop=1;} elist SEMICOLON M expr SEMICOLON
				{	expr_s* tmpBool = newexpr(constbool_e);
					
					// action after L_PARENTH counts for 1
					tmpBool->boolConst = 1;
					$$.test = $6;				// M is #6
					$$.enter = nextquad();
					emit(if_eq, $7, tmpBool, NULL);		// expr is #7
				}
				;

forstmt:		forprefix N elist R_PARENTH N loopstmt N
				{	foundLoop = 0;
					printf("for (elist;expr;elist) stmt\n");
					
					patchlabel($1.enter, $5 + 2);	// true jump
					patchlabel($2, nextquad() + 1);	// false jump
					patchlabel($5, $1.test + 1);	// loop jump
					patchlabel($7, $2 + 2);			// closure jump
					
					//break_s *bList = $6.breakList;
					//continue_s *cList = $6.continueList;
					
					break_s *list1 = bList;
					continue_s *list2 = cList;
					
					while (list1 != NULL) {
						patchlabel(list1->nextquad, nextquad() + 1);	//false jump
						list1 = list1 -> next;
					}
					
					while (list2 != NULL) {
						patchlabel(list2->nextquad, $2 + 3);		// closure jump
						list2 = list2 -> next;
					}
					
					// TODO: empty lists
					list1 = bList;
					while (list1 != NULL){
						break_s *tmp = list1;
						list1 = list1 -> next;
						free(tmp);
					}
					bList = NULL;
					
					list2 = cList;
					while (list2 != NULL){
						continue_s *tmp = list2;
						list2 = list2 -> next;
						free(tmp);
					}
					cList = NULL;
					
				}
				;

				
returnstmt:		RETURN SEMICOLON			{printf("return;\n"); emit(ret,NULL,NULL,NULL);}
				| RETURN expr SEMICOLON		{printf("return expr;\n"); emit(ret,$2,NULL,NULL);}
				;
%%

static void token_value (FILE *file, int type, const YYSTYPE *value){
	switch (type){
		case ID:
			fprintf(file, "%s", value->strValue);
			break;
		case INTEGER:
			fprintf(file, "%d", value->intValue);
			break;
		case FLOAT:
			fprintf(file, "%f", value->floatValue);
			break;
	}
}

int yyerror (char* yaccProvideMessage) {

	if (yychar == YYEOF){
		fprintf(stderr,"%d: EOF accured.\n", yylineno);
		return 0;
	}

	fprintf(stderr,"%s: at line %d,before token: \"%s\"\n", 
			yaccProvideMessage, yylineno, yytext);
	
	fprintf(stderr,"INPUT NOT VALID\n");
	return 0;

}

int main(int argc, char** argv) {
	if(argc>1) {
		if( !(yyin = fopen(argv[1],"r"))) {
			fprintf(stderr, "Cannot read file: %s\n", argv[1]);
			return 1;
		}
	}else{
		yyin = stdin;
	}
	createHTable(&symTable);
	//yydebug = 1;
	yyparse();
	printHashTable(symTable);
	if (foundError == 0){
		print_quads("quads.txt");
		genInstruction();
		print_instructions();
		execute_code();
	}else{
		FILE *out_quad = fopen("quads.txt","w");
		fprintf(stderr, "%d Errors found. Skipping...\n",foundError);
		fprintf(out_quad, " ");
		fclose(out_quad);
	}
	return 0;
}
