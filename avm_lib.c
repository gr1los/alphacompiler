
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hash_table.h"
#include "quads_lib.h"
#include "avm_lib.h"
#include "arithmetic_im.h"
#include "func_im.h"
#include "eq_im.h"
#include "table_im.h"


#define AVM_TABLE_HASHSIZE 211
#define AVM_MAX_INSTRUCTIONS (unsigned) jump_v
#define AVM_WIPEOUT(m)	memset(&(m), 0, sizeof(m))
#define AVM_STACKSIZE	4096


#define AVM_STACKENV_SIZE 4
avm_memcell ax, bx, cx;
avm_memcell retval;
avm_memcell	stack[AVM_STACKSIZE];
unsigned top = AVM_STACKSIZE;
unsigned topsp = AVM_STACKSIZE;

unsigned char	executionFinished = 0;
unsigned		pc = 0;
unsigned 		currLine = 0;
unsigned		codeSize = 0;
instruction*	code = (instruction*) 0;
#define AVM_ENDING_PC codeSize

/* Reverse translation for constants
 * getting constant value from index
 */
double consts_getnumber (unsigned index){
	list_node * curr = doubleList;
	unsigned i;
	for (i=0; i<index; i++){
		curr = curr->next;
	}
	//return *((double*)doubleList[index].node);
	double d = *((double*)curr->node);
	return d;
}
char* consts_getstring (unsigned index){
	//return (char*)stringList[index].node;
	list_node * curr = stringList;
	unsigned i;
	for (i=0; i<index; i++){
		curr = curr->next;
	}
	return (char*)curr->node;
}
char* libfuncs_getused (unsigned index){
	list_node * curr = funcList;
	unsigned i;
	for (i=0; i<index; i++){
		curr = curr->next;
	}
	return (char*)curr->node;
	//return (char*)funcList[index].node;
}


avm_memcell* avm_translate_operand(vmarg* arg, avm_memcell* reg){
	switch (arg->type) {
		/* Variables */
		case global_a:	return &stack[AVM_STACKSIZE - 1 - arg->val];
		case local_a:	return &stack[topsp-arg->val];
		case formal_a:	return &stack[topsp+AVM_STACKENV_SIZE+1+arg->val];
		
		case retval_a:	{return &retval;}
		
		case number_a: {
			reg->type = number_m;
			reg->data.numVal = consts_getnumber(arg->val);
			return reg;
		}
		
		case string_a: {
			reg->type = string_m;
			reg->data.strVal = strdup(consts_getstring(arg->val));
			return reg;
		}
		
		case bool_a: {
			reg->type = bool_m;
			reg->data.boolVal = arg->val;
			return reg;
		}
		
		case nil_a: {
			reg->type = nil_m; 
			return reg;
		}
		
		case userfunc_a: {
			reg->type = userfunc_m;
			reg->data.funcVal = arg->val;
			return reg;
		}
		
		case libfunc_a: {
			reg->type = libfunc_m;
			reg->data.libfuncVal = libfuncs_getused(arg->val);
			return reg;
		}
		
		case label_a: {
			reg->type = number_m;
			reg->data.numVal = arg->val;
			return reg;
		}
		
		default: assert(0);
	}
}

unsigned execute_cycle(void){
	
	if (executionFinished){
		return 1;
		
	}else if (pc == AVM_ENDING_PC){
		executionFinished = 1;
		return 1;
		
	}else{
		assert(pc < AVM_ENDING_PC);
		instruction* instr = code + pc;
		assert(	instr->opcode >= 0 &&
				instr->opcode <= AVM_MAX_INSTRUCTIONS
		);
		if (instr->srcLine)
			currLine = instr->srcLine;
			
		unsigned oldPC = pc;
		(*executeFuncs[instr->opcode])(instr);
		
		if (pc == oldPC)
			++pc;
			
		return 0;
	}
}

void execute_code(void){
	code = instructions;
	codeSize = currInstr;
	
	
	avm_initstack();	// init vm stack
	
	// register libfuncs here
	avm_registerlibfunc("print");
	
	printf("\n==CONSOLE==\n");
	while (1){
		unsigned code = execute_cycle();
		if (code == 1){
			break;
		}
	}
	printf("==END OF CODE==\n");
}

void avm_memcellclear(avm_memcell* m){
	if (m->type != undef_m){
		memclear_func_t f = memclearFuncs[m->type];
		if (f)
			(*f)(m);
		m->type = undef_m;
	}
}
void memclear_string(avm_memcell* m){
	assert(m->data.strVal);
	//free(m->data.strVal);
	m->data.strVal = NULL;
}
void memclear_table(avm_memcell* m){
	assert(m->data.tableVal);
	free(m->data.tableVal);
}

void avm_tablebucketsinit(avm_table_bucket** p){
	unsigned i;
	for (i=0; i<AVM_TABLE_HASHSIZE; i++)
		p[i] = (avm_table_bucket*) 0;
}
avm_table* avm_tablenew(void){
	avm_table* t = (avm_table*)malloc(sizeof(avm_table));
	AVM_WIPEOUT(*t);
	
	t->refCounter = t->total = 0;
	avm_tablebucketsinit(t->numIndexed);
	avm_tablebucketsinit(t->strIndexed);
	
	return t;
}

void avm_tablebucketsdestroy (avm_table_bucket** p){
	unsigned i;
	avm_table_bucket* b;
	for(i=0; i<AVM_TABLE_HASHSIZE; ++i, ++p) {
		for(b = *p; b;){
			avm_table_bucket* del = b;
			b = b->next;
			avm_memcellclear(&del->key);
			avm_memcellclear(&del->value);
			free(del);
		}
		p[i] = (avm_table_bucket*) 0;
	}
}
void avm_tabledestroy (avm_table* t) {
	avm_tablebucketsdestroy (t->strIndexed);
	avm_tablebucketsdestroy (t->numIndexed);
	free(t);
}

avm_memcell* avm_tablegetelem(avm_table* t,avm_memcell* index){
	return NULL;
}
void avm_tablesetelem(avm_table* t, avm_memcell* index, avm_memcell* content){
	return;
}

void avm_initstack(void){
	unsigned i;
	for (i=0; i<AVM_STACKSIZE; i++){
		AVM_WIPEOUT(stack[i]);
		stack[i].type = undef_m;
	}
}

void avm_tableincrefcounter(avm_table* t){
	++t->refCounter;
}
void avm_tabledecrefcounter(avm_table* t){
	assert(t->refCounter > 0);
	if(!--t->refCounter)
		avm_tabledestroy(t);
}

// AVM INSTRUCTION FUNCTIONS
void avm_assign(avm_memcell* lv, avm_memcell* rv){
	if (lv == rv)	// same cells? destructive to assign
		return;
		
	if (lv->type == table_m &&	//same tables? no need to assign
		rv->type == table_m &&
		lv->data.tableVal == rv->data.tableVal)
		return;
		
	if (rv->type == undef_m)	//from undef rvalue ? warn!
		fprintf(stderr, "assigning from undef content!\n");
		
	avm_memcellclear(lv);	//clear old cell contents
	
	memcpy(lv, rv, sizeof(avm_memcell));
	
	//now take care of copied values or reference counting
	if (lv->type == string_m){
		lv->data.strVal = strdup(rv->data.strVal);
	}else if (lv->type == table_m){
		avm_tableincrefcounter(lv->data.tableVal);
	}
}

void avm_registerlibfunc(char* id){
	SymbolTableEntry* libF = searchHashTable(symTable,id,0);
	
	if (libF == NULL){
		fprintf(stderr,"Libfunction '%s' not found in symtable!\n",id);
	}
	
	addFuncUser(libF);
}



// EXECUTE INSTRUCTION FUNCTIONS
void execute_assign(instruction* instr){
	avm_memcell* lv = avm_translate_operand(&instr->result, (avm_memcell*)0);
	avm_memcell* rv = avm_translate_operand(&instr->arg2, &ax);
	
	//assert(lv && (&stack[AVM_STACKSIZE-1] >= lv && lv > &stack[top] || lv == &retval));
	assert(rv);
	
	avm_assign(lv, rv);
}


