#ifndef QUADS_LIB_H_
#define QUADS_LIB_H_

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>


#define MAX_NESTED_FUNCTIONS 100

typedef enum iopcode {
	assign,			add,			sub,
	mul,			divide,			mod,
	uminus,			and,			or,
	not,			if_eq,			if_noteq,
	if_lesseq,		if_greateq,		if_less,
	if_greater,		call,			param,
	ret,			getretval,		funcstart,
	funcend,		tablecreate,	jump,
	tablegetelem,	tablesetelem,	nop
}iopcode;

static const char* iopcodeName[] = {
	"ASSIGN",		"ADD",			"SUB",
	"MUL",			"DIV",			"MOD",
	"UMINUS",		"AND",			"OR",
	"NOT",			"IF_EQ",		"IF_NOTEQ",
	"IF_LESSEQ",	"IF_GREATEQ",	"IF_LESS",
	"IF_GREATER",	"CALL",			"PARAM",
	"RET",			"GETRETVAL",	"FUNCSTART",
	"FUNCEND",		"TABLECREATE",	"JUMP",
	"TABLEGETELEM",	"TABLESETELEM",	"NOP"
};

typedef enum expr_e{
	var_e,
	tableitem_e,
	
	programfunc_e,
	libraryfunc_e,
	
	arithexpr_e,
	boolexpr_e,
	assignexpr_e,
	newtable_e,
	
	constnum_e,
	constbool_e,
	conststring_e,
	
	nil_e,
}expr_e;

typedef struct expr_s {
	expr_e							type;
	struct SymbolTableEntry_t*		sym;
	struct expr_s*					index;
	double							numConst;
	char*							strConst;
	unsigned char					boolConst;
	struct expr_s*					next;
}expr_s;

typedef struct quad {
	iopcode		op;
	expr_s		*result;
	expr_s		*arg1;
	expr_s		*arg2;
	unsigned	label;
	unsigned	line;
	unsigned	taddress;
}quad;

typedef struct forstmt_s{
	int test;
	int enter;
}forstmt_s;

typedef struct callsuffix_s{
	expr_s*			elist;
	unsigned char	method;
	char*			name;
}callsuffix_s;

typedef struct indexelem_s{
	expr_s*		first;
	expr_s*		second;
}indexelem_s;

typedef struct stack_s{
	unsigned int	data[MAX_NESTED_FUNCTIONS];
	unsigned int	len;
}stack_s;

typedef struct break_s{
	int nextquad;
	struct break_s* next;
}break_s;

typedef struct continue_s{
	int nextquad;
	struct continue_s* next;
}continue_s;

typedef struct stmt_s{
	break_s		*breakList;
	continue_s	*continueList;
}stmt_s;

typedef struct list_s{
	unsigned int label;
	struct list_s* next;
}list_s;

typedef struct list_node{
	void* node;
	unsigned index;
	struct list_node* next;
}list_node;

static const char* vmopcodeName[] = {
	"ASSIGN_V",			"ADD_V",			"SUB_V",
	"MUL_V", 			"DIV_V",			"MOD_V",
	"UMINUS_V", 		"AND_V",			"OR_V",
	"NOT_V", 			"JEQ_V",			"JNE_V",
	"JLE_V",			"JGE_V",			"JLT_V",
	"JGT_V",			"CALL_V",			"PUSHARG_V",
	"FUNCENTER_V",		"FUNCEXIT_V",		"NEWTABLE_V",
	"TABLEGETELEM_V", 	"TABLESETELEM_V", 	"NOP_V",
	"JUMP_V"
};

typedef enum vmopcode {
	assign_v,		add_v,			sub_v,
	mul_v, 			div_v,			mod_v,
	uminus_v, 		and_v,			or_v,
	not_v, 			jeq_v,			jne_v,
	jle_v,			jge_v,			jlt_v,
	jgt_v,			call_v,			pusharg_v,
	funcenter_v,	funcexit_v,		newtable_v,
	tablegetelem_v, tablesetelem_v, nop_v,
	jump_v
}vmopcode;

typedef void (*generator_func_t) (quad*);

// VMarg structures

typedef enum vmarg_t {
	label_a  = 0,
	global_a = 1,
	formal_a = 2,
	local_a  = 3,
	number_a = 4,
	string_a = 5,
	bool_a 	 = 6,
	nil_a	 = 7,
	userfunc_a = 8,
	libfunc_a  = 9,
	retval_a   =10
}vmarg_t;

typedef struct vmarg { 
	vmarg_t type; 
	unsigned val;
}vmarg;

typedef struct instruction {
	vmopcode	opcode;
	vmarg		result;
	vmarg		arg1;
	vmarg		arg2;
	unsigned	srcLine;
}instruction;

typedef struct incomplete_jump {
	unsigned instrNo; 
	unsigned iaddress;
	struct incomplete_jump* next;
}incomplete_jump;

typedef struct retVal{
	unsigned		retval;
	struct retVal*	next;
}retVal;

typedef struct func{
	struct SymbolTableEntry_t*		sym;
	unsigned 						taddress;
	retVal*							retVals;
	struct func*					next;
}func;

typedef struct stack_v{
	unsigned	len;
	func*	symbols[100];
}stack_v;

typedef struct userFunc{
	struct SymbolTableEntry_t*		sym;
	struct userFunc* next;
}userFunc;

// Quad Functions
void init_quad (char*);

unsigned int nextquad();

unsigned int currquad(void);

void expand (void);

expr_s *newexpr (enum expr_e);

expr_s* lvalue_expr(struct SymbolTableEntry_t*);

expr_s* member_item(expr_s*, char*);

expr_s* make_call(expr_s*, expr_s*);

void emit(iopcode, expr_s*, expr_s*, expr_s*);

expr_s* emit_iftableitem(expr_s*);

void patchlabel(unsigned, unsigned);

int checkuminus(expr_s*);

int istempexpr(expr_s*);

void print_quads(char*);

// Stack Functions

unsigned int pop(stack_s);

void push(stack_s, unsigned int);

// List Functions

break_s* newlist_b(int,break_s*);

continue_s* newlist_c(int,continue_s*);

break_s* merge_b(break_s*,break_s*);

continue_s* merge_c(continue_s*,continue_s*);

// Backpatching functions

list_s* newlist(unsigned int,list_s*);

void backpatch(unsigned int, retVal*);

// Code Generation Functions
void genInstruction();
unsigned insertList(list_node**, list_node*);

unsigned consts_newstring (char*);
unsigned consts_newnumber (double);
unsigned libfuncs_newused (char*);

void make_operand (expr_s*, vmarg*);
void expandUF(void);
void emitI(instruction);
void append(retVal** , unsigned );
int nextInstructionLabel();
void make_numberoperand(vmarg*, double);
void make_booloperand(vmarg*, unsigned);
void make_retvaloperand(vmarg*);

vmarg* reset_operand(vmarg*);

unsigned currprocessedquad();
void add_incomplete_jump(unsigned, unsigned );
void patch_incomplete_jumps();

void pushStack(func*);
func* topStack();
func* popStack();
int addFuncUser(struct SymbolTableEntry_t* );

void generate(vmopcode, quad*);
void generate_relational(vmopcode, quad* );

void generate_ADD (quad*);
void generate_SUB (quad*);
void generate_MUL (quad*);
void generate_DIV (quad*);
void generate_MOD (quad*);
void generate_NEWTABLE (quad*);
void generate_TABLEGETELEM (quad*);
void generate_TABLESETELEM (quad*);
void generate_ASSIGN (quad*);
void generate_NOP (quad*);
void generate_JUMP (quad*);
void generate_IF_EQ (quad*);
void generate_IF_NOTEQ (quad*);
void generate_IF_GREATER (quad*);
void generate_IF_GREATEREQ (quad*);
void generate_IF_LESS (quad*);
void generate_IF_LESSEQ (quad*);
void generate_NOT (quad*);
void generate_OR (quad*);
void generate_AND (quad*);
void generate_PARAM (quad*);
void generate_CALL (quad*);
void generate_GETRETVAL (quad*);
void generate_FUNCSTART (quad*);
void generate_RETURN (quad*);
void generate_FUNCEND (quad*);
void generate_UMINUS (quad*);

extern instruction*		instructions;
extern unsigned int 	currInstr;
extern int				currUserFuncSize;
// Lists for code generation
extern list_node* stringList;
extern list_node* doubleList;
extern list_node* funcList;

#endif
