#include "quads_lib.h"
#include "hash_table.h"

quad*			quads = (quad*) 0;
instruction*	instructions = (instruction*) 0;
unsigned		total = 0;
unsigned 		totalInstr = 0;
unsigned int	currQuad = 0;
unsigned int 	currInstr = 0;
incomplete_jump* ij_head = NULL;
unsigned		ij_total = 0;
unsigned		prQuad = 0;
stack_v			funcStack;
int				userFuncSize;
int				currUserFuncSize;
userFunc*		userFL = (userFunc*) 0;

FILE* out_quad;						// quad output file

// Lists for code generation
list_node* stringList = NULL;
list_node* doubleList = NULL;
list_node* funcList = NULL;


#define EXPAND_SIZE 1024
#define CURR_SIZE	(total*sizeof(quad))
#define NEW_SIZE	(EXPAND_SIZE*sizeof(quad)+CURR_SIZE)
#define CURR_ISIZE	(totalInstr*sizeof(instruction))
#define NEW_ISIZE	(EXPAND_SIZE*sizeof(instruction)+CURR_ISIZE)
#define CURR_FSIZE	(userFuncSize*sizeof(userFunc))
#define NEW_FSIZE	(EXPAND_SIZE*sizeof(userFunc)+CURR_FSIZE)
#define FALSE		0
#define TRUE		1

void init_quad(char* file){
	out_quad = fopen(file,"w");
}

unsigned int nextquad(void){
	return currQuad;
}

unsigned int currquad(void){
	return currQuad - 1;
}

void expand(void) {
	quad* p = (quad*) malloc (NEW_SIZE);
	if (quads) {
		memcpy(p, quads, CURR_SIZE);
		free(quads);
	}
	quads = p;
	total += EXPAND_SIZE;
}

expr_s* newexpr(enum expr_e type){
	expr_s* newexpr;
	
	newexpr = (expr_s*)malloc(sizeof(expr_s));
	
	if ( newexpr == NULL) {
		fprintf(stderr,"Error on malloc\n");
		return NULL;
	}
	newexpr->sym = NULL;
	newexpr->type = type;
	
	return newexpr;
}

expr_s* lvalue_expr(struct SymbolTableEntry_t* sym){
	expr_s* e = (expr_s*)malloc(sizeof(expr_s)); 
	
	if ( e == NULL) {
		fprintf(stderr,"Error on malloc\n");
		return NULL;
	}
	
	memset(e, 0, sizeof(expr_s));
	e->next = NULL;
	e->sym = sym;
	
	switch (sym->type) {
		case GLOBAL_V	: {e->type = var_e; break;}
		case LOCAL_V	: {e->type = var_e; break;}
		case FORMAL_V	: {e->type = var_e; break;}
		case USERFUNC	: {e->type = programfunc_e; break;}
		case LIBFUNC	: {e->type = libraryfunc_e; break;}
		default			: {printf("lvalue_expr\n"); break;}
	}
	return e;
}

expr_s* member_item(expr_s* lvalue, char* name){
	expr_s *e, *item;
	e = emit_iftableitem(lvalue);
	item = newexpr(tableitem_e);
	item -> sym = e->sym;
	item -> index = newexpr(conststring_e);
	item -> index -> strConst = name;
	return item;
}

expr_s* make_call(expr_s* lvalue, expr_s* elist){
	expr_s* func = emit_iftableitem(lvalue);
	expr_s* curr = elist;
	expr_s* result;
	
	while(curr != NULL){
		emit(param, curr, NULL, NULL);
		curr = curr -> next;
	}
	
	emit(call, func, NULL, NULL);
	result = newexpr(var_e);
	result->sym = newtemp(symTable);
	emit(getretval, result, NULL, NULL);
	
	return result;
}

//void emit(iopcode opcode, expr_s* res, expr_s* arg1, expr_s* arg2, unsigned label, unsigned line){
void emit(iopcode opcode, expr_s* res, expr_s* arg1, expr_s* arg2){
	if (currQuad == total)
		expand();
	
	quad* p = quads+currQuad++;
	p->op = opcode;
	p->result = res;
	p->arg1 = arg1;
	p->arg2 = arg2;
	p->line = yylineno;
}

expr_s* emit_iftableitem(expr_s* e){
	expr_s* result;
	
	if (e->type != tableitem_e){
		return e;
	}else{
		result = newexpr(var_e);
		result->sym = newtemp(symTable);
		emit(tablegetelem, result, e, e->index);
		return result;
	}
}

void patchlabel(unsigned quadNo, unsigned label){
	quads[quadNo].label = label;
}

int checkuminus(expr_s* expr){
	if (expr -> type == constbool_e		||	// illegal uminus expr
		expr -> type == conststring_e	||
		expr -> type == nil_e			||
		expr -> type == newtable_e		||
		expr -> type == programfunc_e	||
		expr -> type == libraryfunc_e	||
		expr -> type == boolexpr_e){
			
		return 0;
	}
	return 1;
}

int istempexpr(expr_s* expr){
	return (expr->sym				&& 
			expr->sym->type == var_e	&& 
			istempname(getSTEName(expr->sym)));
}

void print_quads(char* file){
	out_quad = fopen(file,"w");
	int i;
	for (i=0 ;i<currQuad; i++){
		//printf("%d:\t%s\n",i+1,iopcodeName[(int)quads[i].op]);
		fprintf(out_quad,"%d:\t%s ",i+1,iopcodeName[(int)quads[i].op]);
		
		if (quads[i].op == jump) {
			fprintf(out_quad,"%d\n", quads[i].label);
			continue;
		}
		
		if (quads[i].result->type == constbool_e){
			fprintf(out_quad,"%s ",quads[i].result->boolConst?"TRUE":"FALSE");
		}else if (quads[i].result->type == constnum_e){
			fprintf(out_quad,"%g ",quads[i].result->numConst);
		}else if (quads[i].result->type == conststring_e){
			fprintf(out_quad,"%s ",quads[i].result->strConst);
		}else{
			fprintf(out_quad,"%s ",getSTEName(quads[i].result->sym));
		}
		
		if (quads[i].arg1 != NULL){
			if (quads[i].arg1->type == constbool_e){
				fprintf(out_quad,"%s ",quads[i].arg1->boolConst?"TRUE":"FALSE");
			}else if (quads[i].arg1->type == constnum_e){
				fprintf(out_quad,"%g ",quads[i].arg1->numConst);
			}else if (quads[i].arg1->type == conststring_e){
				fprintf(out_quad,"%s ",quads[i].arg1->strConst);
			}else if (quads[i].arg1->type == nil_e){
				fprintf(out_quad,"null ");
			}else{
				fprintf(out_quad,"%s ",getSTEName(quads[i].arg1->sym));
			}
		}
		
		if (quads[i].op == if_eq || quads[i].op == if_noteq || quads[i].op == if_lesseq || 
			quads[i].op == if_greateq || quads[i].op == if_less || quads[i].op == if_greater){
			
			fprintf(out_quad,"%d\n",quads[i].label);
			continue;
		}
		
		if (quads[i].arg2 != NULL){
			if (quads[i].arg2->type == constbool_e){
				fprintf(out_quad,"%s ",quads[i].arg2->boolConst?"TRUE":"FALSE");
			}else if (quads[i].arg2->type == constnum_e){
				fprintf(out_quad,"%g ",quads[i].arg2->numConst);
			}else if (quads[i].arg2->type == conststring_e){
				fprintf(out_quad,"%s ",quads[i].arg2->strConst);
			}else if (quads[i].arg2->type == nil_e){
				fprintf(out_quad,"null ");
			}else{
				fprintf(out_quad,"%s ",getSTEName(quads[i].arg2->sym));
			}
		}
		fprintf(out_quad,"\n");
	}
	fprintf(out_quad,"%d:\t ",i+1);
	fclose(out_quad);
}

void print_instructions(){
	unsigned i;
	printf("\n===ICODE===\n\n");
	for (i=0; i<currInstr; i++){
		printf("%s %d %d %d\n",vmopcodeName[instructions[i].opcode], instructions[i].result.val, instructions[i].arg1.val, instructions[i].arg2.val);
	}
}

// Stack Functions

unsigned int pop(stack_s stack){
	if (stack.len == 0) {
		fprintf(stderr,"Cannot pop empty stack\n");
		return 0;
	}
	return stack.data[stack.len--];
}

void push(stack_s stack, unsigned int elem){
	if (stack.len == MAX_NESTED_FUNCTIONS) {
		fprintf(stderr,"Cannot push full stack\n");
		return;
	}
	stack.data[stack.len++] = elem;
}

// List Functions

break_s* newlist_b(int nextquad,break_s* list){
	break_s* current = list;
	break_s* newNode;
	
	if(list == NULL){
		newNode = (break_s*)malloc(sizeof(break_s));
		newNode -> nextquad = nextquad;
		newNode -> next = NULL;
		return newNode;
	}
	while(current->next != NULL){
		current = current->next;
	}
	newNode = (break_s*)malloc(sizeof(break_s));
	newNode -> nextquad = nextquad;
	newNode -> next = NULL;
	
	current->next = newNode;
	return list;
}

/*
break_s* newlist_b(int nextquad,break_s* list){
	break_s* current = list;
	printf("nextquad = %d\n",nextquad);
	if(list == NULL){
		list = (break_s*)malloc(sizeof(break_s));
		list->nextquad = nextquad;
		list->next = NULL;
		return list;
	}
	while(current->next != NULL){
		current = current->next;
	}
	current->next = (break_s*)malloc(sizeof(break_s));
	current->next->nextquad = nextquad;
	current->next->next = NULL;
	return list;
}
*/

continue_s* newlist_c(int nextquad,continue_s* list){
	continue_s* current = list;
	if(list == NULL){
		list = (continue_s*)malloc(sizeof(continue_s));
		list->nextquad = nextquad;
		list->next = NULL;
		return list;
	}
	while(current->next != NULL){
		current = current->next;
	}
	current->next = (continue_s*)malloc(sizeof(continue_s));
	current->next->nextquad = nextquad;
	current->next->next = NULL;
	return list;
}

break_s* merge_b(break_s* list1, break_s* list2){
	break_s* cur = list1;

	if((list1 == NULL) && (list2 != NULL)){
		return list2;
	}
	if((list1 == NULL) && (list2 == NULL)){
		return NULL;
	}
	if((list1 != NULL) && (list2 == NULL)){
		return list1;
	}
	while(cur != NULL){
		cur = cur->next;
	}
	
	cur = list2;
	
	return list1;
} 

continue_s* merge_c(continue_s* list1, continue_s* list2){
	continue_s* cur = list1;
	
	if((list1 == NULL) && (list2 != NULL)){
		list1 = list2;
		return list1;
	}
	if((list1 == NULL) && (list2 == NULL)){
		return list1;
	}
	if((list1 != NULL) && (list2 == NULL)){
		return list1;
	}
	while(cur->next != NULL){
		cur = cur->next;
	}
	cur->next = list2;
	
	return list1;
} 

// Backpatching functions

list_s* newlist(unsigned int label,list_s* list){
	list_s* current = list;
	if(list == NULL){
		list = (list_s*)malloc(sizeof(list_s));
		list->label = label;
		list->next = NULL;
		return list;
	}
	while(current->next != NULL){
		current = current->next;
	}
	current->next = (list_s*)malloc(sizeof(list_s));
	current->next->label = label;
	current->next->next = NULL;
	return list;
}

void backpatch(unsigned int label, retVal* list){
	retVal* curr = list;
	while (curr != NULL){
		curr->retval = label;
		curr = curr -> next;
	}
	
}

// Code Generation Functions
generator_func_t generators[] = {
	generate_ASSIGN,
	generate_ADD, 
	generate_SUB, 
	generate_MUL,
	generate_DIV,
	generate_MOD,
	generate_UMINUS,
	generate_AND,
	generate_OR,
	generate_NOT,
	generate_IF_EQ,
	generate_IF_NOTEQ,
	generate_IF_LESSEQ,
	generate_IF_GREATEREQ,
	generate_IF_LESS,
	generate_IF_GREATER,
	generate_CALL,
	generate_PARAM,
	generate_RETURN,
	generate_GETRETVAL,
	generate_FUNCSTART,
	generate_FUNCEND,
	generate_NEWTABLE,
	generate_JUMP,
	generate_TABLEGETELEM,
	generate_TABLESETELEM,
	generate_NOP
};

void genInstruction(void) {
	unsigned i = 0;
	for (i=0; i<currQuad; ++i){
		prQuad++;
		(*generators[quads[i].op])(quads+i);
	}	
}

unsigned insertList(list_node** head, list_node* n){

	list_node* cur = (*head);
	if((*head) == NULL){
		//(*head) = (list_node*)malloc(sizeof(list_node));
		//(*head)->node = n->node;
		(*head) = n;
		(*head)->index = 0;
		(*head)->next = NULL;
		return (*head)->index;
	}
	
	while(cur->next != NULL){
		cur = cur->next;
	}
	cur->next = n;
	n->index = cur->index + 1;
	n->next = NULL;
	//cur = cur->next;
	//return cur->index;
	return n->index;
}


unsigned consts_newstring (char* s){
	list_node* str = (list_node*)malloc(sizeof(list_node));
	str->node = s;
	unsigned index = insertList(&stringList,str);
	return index;
}
unsigned consts_newnumber (double d){
	list_node* dbl = (list_node*)malloc(sizeof(list_node));
	double* db_ptr = (double*)malloc(sizeof(double));
	*db_ptr = d;
	dbl->node = db_ptr;
	unsigned index = insertList(&doubleList,dbl);
	return index;
}
unsigned libfuncs_newused (char* s){
	list_node* str = (list_node*)malloc(sizeof(list_node));
	str->node = s;
	unsigned index = insertList(&funcList,str);
	return index;
}

void make_operand (expr_s* e, vmarg* arg) {
	switch (e->type) {
		
		case var_e :
		case tableitem_e :
		case arithexpr_e :
		case boolexpr_e :
		case assignexpr_e:
		case newtable_e : {
			arg->val = getSTEOffset(e->sym);
			
			switch (getSTESpace(e->sym)) {
				
				case programvar		:{
					arg->type = global_a ;
					break;
				}
				case functionlocal	:{
					arg->type = local_a;
					break;
				}
				case formalarg	:{
					arg->type = formal_a;
					break;
				}
				default: assert(0);
			}
			break;
		}
	
		/* Constants */
		
		case constbool_e: {
			arg->val = e->boolConst ;
			arg->type = bool_a ;
			break;
		}
		
		case conststring_e: {
			arg->val = consts_newstring(e->strConst);
			arg->type = string_a;
			break;
		}
		
		case constnum_e: {
			arg->val = consts_newnumber(e->numConst);
			arg->type = number_a;
			break;
		}
		
		case nil_e:	{
			arg->type = nil_a;
			break;
		}
		
		/* Functions */
		
		case programfunc_e: {
			arg->type = userfunc_a;
			arg->val = e->sym->value.funcVal->iaddress;
			break;
		}
		
		case libraryfunc_e: {
			arg->type = libfunc_a;
			arg->val = libfuncs_newused(getSTEName(e->sym));
			break;
		}
		
		default:	assert(0);
	}
}


void expandUF(void) {
	userFunc* uf = (userFunc*) malloc (NEW_FSIZE);
	if (userFL) {
		memcpy(uf, userFL, CURR_FSIZE);
		free(userFL);
	}
	userFL = uf;
	userFuncSize += EXPAND_SIZE;
}

void expandInstr(void) {
	instruction* p = (instruction*) malloc (NEW_ISIZE);
	if (instructions) {
		memcpy(p, instructions, CURR_ISIZE);
		free(instructions);
	}
	instructions = p;
	totalInstr += EXPAND_SIZE;
}

void emitI(instruction t){
	if (currInstr == totalInstr)
		expandInstr();
	
	instruction* i = instructions+currInstr++;
	i->opcode = t.opcode;
	i->result = t.result;
	i->arg1 = t.arg1;
	i->arg2 = t.arg2;
	i->srcLine = t.srcLine;
}

void append(retVal** head, unsigned label){
	retVal* curr = (*head);
	if((*head) == NULL){
		(*head) = (retVal*)malloc(sizeof(retVal));
		(*head)->retval = label;
		(*head)->next = NULL;
	}
	else{
		while(curr-> next != NULL){
			curr = curr->next;
		}
		curr->next = (retVal*)malloc(sizeof(retVal));
		curr->retval = label;
		curr->next->next = NULL;
	}
}

int nextInstructionLabel(){
	return currInstr;
}

void make_numberoperand (vmarg* arg, double val) {
	arg->val = consts_newnumber(val);
	arg->type = number_a;
}

void make_booloperand (vmarg* arg, unsigned val) {
	arg->val = val;
	arg->type = bool_a;
}

void make_retvaloperand (vmarg* arg) {
	arg->type = retval_a;
}

vmarg* reset_operand(vmarg* t){
   t = NULL;
   return t;
}

unsigned currprocessedquad(){
	return prQuad;
}

void add_incomplete_jump(unsigned addr, unsigned label){
	incomplete_jump* curr = ij_head;
	if(ij_head == NULL){
		ij_head = (incomplete_jump*) malloc(sizeof(struct incomplete_jump));
		ij_head->instrNo = addr;
		ij_head->iaddress = label;
		ij_head->next = NULL;
	}
	else{
		while(curr->next != NULL){
			curr = curr->next;
		}
		curr->next = (incomplete_jump*) malloc(sizeof(struct incomplete_jump));
		curr = curr->next;
		curr->instrNo = addr;
		curr->iaddress = label;
		curr->next = NULL;
	}
}

void patch_incomplete_jumps(){
	incomplete_jump* curr = ij_head;
	while(curr->next != NULL){
		if(curr->iaddress == nextquad())
			instructions[curr->instrNo].result.val = nextInstructionLabel();
		else
			instructions[curr->instrNo].result.val = quads[curr->iaddress].taddress;
		curr = curr->next;	
	}
}

void pushStack(func* f){
	if (funcStack.len == 100) {
		fprintf(stderr,"Cannot push full stack\n");
		return;
	}
	funcStack.symbols[funcStack.len++] = f;
}

func* popStack(){
	if (funcStack.len == 0) {
		fprintf(stderr,"Cannot pop empty stack\n");
		return 0;
	}
	return funcStack.symbols[funcStack.len--];
}

func* topStack(){
	if (funcStack.len == 0) {
		fprintf(stderr,"Cannot pop empty stack\n");
		return 0;
	}
	return funcStack.symbols[funcStack.len - 1];
}

int addFuncUser(struct SymbolTableEntry_t* sym){
	if (currUserFuncSize == userFuncSize)
		expandUF();
	
	if(userFL + currUserFuncSize == NULL)
		return 0;
	
	userFunc* tmp = userFL+(currUserFuncSize++);
	tmp->sym = sym;
	return currUserFuncSize - 1;
}

void generate(vmopcode opcode, quad* q){
	instruction t;
	t.opcode = opcode;
	make_operand(q->result, &t.result);
	if (q->arg1 != NULL)
		make_operand(q->arg1, &t.arg1);
	
	if (q->arg2 != NULL)
		make_operand(q->arg2, &t.arg2);
	
	q->taddress =  nextInstructionLabel();
	t.srcLine = q->line;
	emitI(t);
}

void generate_relational(vmopcode op, quad* q) {
	instruction t;
	t.opcode = op;
	if (q->arg1 != NULL)
		make_operand(q->arg1, &t.arg1);
	if (q->arg2 != NULL)
		make_operand(q->arg2, &t.arg2);
	t.result.type = label_a;
	if(q->label < currprocessedquad())
		t.result.val = quads[q->label].taddress;
	else
		add_incomplete_jump(nextInstructionLabel(), q->label);
	q->taddress = nextInstructionLabel();
	t.srcLine = q->line;
	emitI(t);
}

void generate_ADD (quad* q){ generate(add_v, q); }
void generate_SUB (quad* q){ generate(sub_v, q); }
void generate_MUL (quad* q){ generate(mul_v, q); }
void generate_DIV (quad* q){ generate(div_v, q); }
void generate_MOD (quad* q){ generate(mod_v, q); } 
void generate_NEWTABLE (quad* q){ generate(newtable_v, q); }
void generate_TABLEGETELEM (quad* q){ generate(tablegetelem_v, q); }
void generate_TABLESETELEM (quad* q){ generate(tablesetelem_v, q); }
void generate_ASSIGN (quad* q){ generate(assign_v, q); }
void generate_NOP (quad* q){ instruction t; t.opcode = nop_v; emitI(t); }
void generate_JUMP (quad* q){ generate_relational(jump_v, q); }
void generate_IF_EQ (quad* q){ generate_relational(jeq_v, q); }
void generate_IF_NOTEQ (quad* q){ generate_relational(jne_v, q); }
void generate_IF_GREATER (quad* q){ generate_relational(jgt_v, q); }
void generate_IF_GREATEREQ (quad* q){ generate_relational(jge_v, q); }
void generate_IF_LESS (quad* q){ generate_relational(jlt_v, q); }
void generate_IF_LESSEQ (quad* q){ generate_relational(jle_v, q); }
void generate_NOT (quad* q){ 
	q->taddress = nextInstructionLabel();
	instruction t;
	t.opcode = jeq_v;
	make_operand(q->arg1, &t.arg1);
	make_booloperand(&t.arg2, FALSE);
	t.result.type = label_a;
	t.result.val = nextInstructionLabel()+3;
	t.srcLine = q->line;
	emitI(t);

	t.opcode = assign_v;
	make_booloperand(&t.arg1, FALSE);
	reset_operand(&t.arg2);
	make_operand(q->result, &t.result);
	t.srcLine = q->line;
	emitI(t);

	t.opcode = jump_v;
	reset_operand (&t.arg1);
	reset_operand(&t.arg2);
	t.result.type = label_a;
	t.result.val = nextInstructionLabel()+2;
	t.srcLine = q->line;
	emitI(t);

	t.opcode = assign_v;
	make_booloperand(&t.arg1, TRUE);
	reset_operand(&t.arg2);
	make_operand(q->result, &t.result);
	t.srcLine = q->line;
	emitI(t);
}
void generate_OR (quad* q){
	q->taddress = nextInstructionLabel();
	instruction t;
	
	t.opcode = jeq_v;
	make_operand(q->arg1, &t.arg1);
	make_booloperand(&t.arg2, TRUE);
	t.result.type = label_a;
	t.result.val = nextInstructionLabel()+4;
	t.srcLine = q->line;
	emitI(t);

	make_operand(q->arg2, &t.arg1);
	t.result.val = nextInstructionLabel()+3;
	emitI(t);
	
	make_booloperand(&t.arg1, FALSE);
	reset_operand(&t.arg2);
	make_operand(q->result, &t.result);
	t.srcLine = q->line;
	emitI(t);

	t.opcode = jump_v;
	reset_operand (&t.arg1);
	reset_operand(&t.arg2);
	t.result.type = label_a;
	t.result.val = nextInstructionLabel()+2;
	t.srcLine = q->line;
	emitI(t);

	t.opcode = assign_v;
	make_booloperand(&t.arg1, TRUE);
	reset_operand(&t.arg2);
	make_operand(q->result, &t.result);
	t.srcLine = q->line;
	emitI(t);
}
void generate_AND(quad* q){
	q->taddress = nextInstructionLabel();
	instruction t;
	
	t.opcode = jeq_v;
	make_operand(q->arg1, &t.arg1);
	make_booloperand(&t.arg2, FALSE);
	t.result.type = label_a;
	t.result.val = nextInstructionLabel()+4;
	t.srcLine = q->line;
	emitI(t);
	
	t.opcode = jeq_v;
	make_operand(q->arg2, &t.arg1);
	make_booloperand(&t.arg2, FALSE);
	t.result.type = label_a;
	t.result.val = nextInstructionLabel()+3;
	t.srcLine = q->line;
	emitI(t);
	
	t.opcode = assign_v;
	make_booloperand(&t.arg1, TRUE);
	reset_operand(&t.arg2);
	make_operand(q->result, &t.result);
	t.srcLine = q->line;
	emitI(t);
	
	t.opcode = jump_v;
	reset_operand (&t.arg1);
	reset_operand(&t.arg2);
	t.result.type = label_a;
	t.result.val = nextInstructionLabel()+2;
	t.srcLine = q->line;
	emitI(t);

	t.opcode = assign_v;
	make_booloperand(&t.arg1, FALSE);
	reset_operand(&t.arg2);
	make_operand(q->result, &t.result);
	t.srcLine = q->line;
	emitI(t);
}
void generate_PARAM (quad* q){
	q->taddress = nextInstructionLabel();
	instruction t;
	t.opcode = pusharg_v;
	make_operand(q->result, &t.arg1);
	t.srcLine = q->line;
	emitI(t);
}
void generate_CALL (quad* q){
	q->taddress = nextInstructionLabel();
	instruction t;
	t.opcode = call_v;
	make_operand(q->result, &t.arg1);
	t.srcLine = q->line;
	emitI(t);
}
void generate_GETRETVAL (quad* q){
	q->taddress = nextInstructionLabel();
	instruction t;
	t.opcode = assign_v;
	make_operand(q->result, &t.arg2);
	make_retvaloperand(&t.result);
	t.srcLine = q->line;
	emitI(t);
}
void generate_FUNCSTART (quad* q){
	func* f;
	f = (func*)malloc(sizeof(func*));
	f->sym = q->result->sym;
	f->taddress = nextInstructionLabel();
	q->taddress = nextInstructionLabel();
	
	instruction t;

	t.opcode = jump_v;
   	reset_operand(&t.arg1);
   	reset_operand(&t.arg2);
   	t.result.type = label_a;
   	t.result.val = nextInstructionLabel();
	t.srcLine = q->line;
	emitI(t);

   	struct SymbolTableEntry_t* sym = q->result->sym;
	q->taddress = nextInstructionLabel();
	sym-> value.funcVal -> iaddress = nextInstructionLabel();
	sym-> value.funcVal -> line = 0;
	addFuncUser(sym); 
	pushStack(f);
	
	t.opcode = funcenter_v;
	make_operand(q->result, &t.result);
	reset_operand(&t.arg1);
	reset_operand(&t.arg2);
	t.srcLine = q->line;
	emitI(t);
}
void generate_RETURN (quad* q){
	q->taddress = nextInstructionLabel();
	instruction t;
	t.opcode = assign_v;
	make_retvaloperand(&t.result);
	
	if (q->arg1 != NULL)
		make_operand(q->arg1,&t.arg1);
	t.srcLine = q->line;
	emitI(t);
	
	func* f;
	f = topStack();
	append(&f->retVals, nextInstructionLabel());
	
	t.opcode = jump_v;
	reset_operand(&t.arg1);
	reset_operand(&t.arg2);
	t.result.type = label_a;
	t.srcLine = q->line;
	emitI(t);
}
void generate_FUNCEND (quad* q){
	func* f;
	f = topStack();
	backpatch(nextInstructionLabel(), f->retVals);
	
	q->taddress = nextInstructionLabel();
	instruction t;
	t.opcode = funcexit_v;
	make_operand(q->result, &t.result);
	t.srcLine = q->line;
	emitI(t);
}

void generate_UMINUS (quad* q){
	q->arg2 = (expr_s*)malloc(sizeof(expr_s));
	q->arg2->type = constnum_e;
	q->arg2->numConst = -1;
	generate_MUL(q);
}
