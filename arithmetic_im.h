#ifndef ARITHMETIC_IM_H_
#define ARITHMETIC_IM_H_
#define AVM_STACKSIZE	4096

extern avm_memcell ax, bx, cx;
extern avm_memcell retval;
extern avm_memcell stack[AVM_STACKSIZE];
extern unsigned top;
extern unsigned char	executionFinished;

void execute_arithmetic(instruction* );


typedef double (*arithmetic_func_t)(double, double);

double add_impl(double, double);
double sub_impl(double, double);
double mul_impl(double, double);
double div_impl(double, double);
double mod_impl(double, double);

// Dispatcher for specific functions
static arithmetic_func_t arithmeticFuncs[] = {
	add_impl,
	sub_impl,
	mul_impl,
	div_impl,
	mod_impl
};

#endif
