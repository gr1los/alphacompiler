#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "quads_lib.h"
#include "avm_lib.h"
#include "eq_im.h"


unsigned char number_tobool (avm_memcell* m)   { return m->data.numVal != 0; }
unsigned char string_tobool (avm_memcell* m)   { return m->data.strVal[0] != 0; }
unsigned char bool_tobool (avm_memcell* m)     { return m->data.boolVal; }
unsigned char table_tobool (avm_memcell* m)    { return 1; }
unsigned char userfunc_tobool (avm_memcell* m) {return 1; }
unsigned char libfunc_tobool (avm_memcell* m) { return 0; }
unsigned char nil_tobool (avm_memcell* m)	   { return 0; }
unsigned char undef_tobool (avm_memcell* m)    { assert(0); return 0; }

unsigned char avm_tobool (avm_memcell* m) {
	assert (m->type >= 0 && m->type < undef_m);
	return (*toboolFuncs[m->type]) (m);
}

void execute_jeq (instruction* instr) {
	assert(instr -> result.type == label_a);
	
	avm_memcell* rv1 = avm_translate_operand (&instr->arg1, &ax);
	avm_memcell* rv2 = avm_translate_operand (&instr->arg2, &bx);
	
	unsigned char result = 0;

	if (rv1->type == undef_m || rv2->type == undef_m){
		fprintf(stderr,"'undef' involved in equality!\n");
		executionFinished = 1;
	}
	else if(rv1->type == nil_m || rv2->type == nil_m)
		result = rv1->type == nil_m && rv2->type == nil_m;
	else if(rv1->type == bool_m || rv2->type == bool_m)
		result = (avm_tobool(rv1) == avm_tobool(rv2));
	else if (rv1->type != rv2->type){
		fprintf(stderr, "%s == %s is illegal!\n", 
			typeStrings[rv1->type], typeStrings[rv2->type]);
		executionFinished = 1;	
	}		
	else {
		switch(rv1->type){
			case number_m: { 
				result = rv1->data.numVal == rv2->data.numVal;
				break;
			}
			case string_m: {
				int c = strcmp(rv1->data.strVal,rv2->data.strVal);
				if(c == 0)
					result = 1;
				break;
			}
			case table_m:{
				result = rv1->data.tableVal == rv2->data.tableVal;
				break;
			}
			case userfunc_m:{
				result = rv1->data.funcVal == rv2->data.funcVal;
				break;
			}
			case libfunc_m: {
				int c = strcmp(rv1->data.libfuncVal,rv2->data.libfuncVal);
				if(c == 0)
					result = 1;
				break;
			}
			default: assert(0);
		}
	}
	
	if(!executionFinished && result)
		pc = instr->result.val;
}
	
void execute_jne (instruction* instr) {
	assert(instr -> result.type == label_a);
	
	avm_memcell* rv1 = avm_translate_operand (&instr->arg1, &ax);
	avm_memcell* rv2 = avm_translate_operand (&instr->arg2, &bx);
	
	unsigned char result = 0;

	if (rv1->type == undef_m || rv2->type == undef_m){
		fprintf(stderr,"'undef' involved in equality!\n");
		executionFinished = 1;
	}
	else if(rv1->type == nil_m || rv2->type == nil_m)
		result = rv1->type == nil_m && rv2->type == nil_m;
	else if(rv1->type == bool_m || rv2->type == bool_m)
		result = (avm_tobool(rv1) == avm_tobool(rv2));
	else if (rv1->type != rv2->type){
		fprintf(stderr, "%s == %s is illegal!\n", 
			typeStrings[rv1->type], typeStrings[rv2->type]);
		executionFinished = 1;	
	}		
	else {
		switch(rv1->type){
			case number_m: { 
				result = rv1->data.numVal == rv2->data.numVal;
				break;
			}
			case string_m: {
				int c = strcmp(rv1->data.strVal,rv2->data.strVal);
				if(c == 0)
					result = 1;
				break;
			}
			case table_m:{
				result = rv1->data.tableVal == rv2->data.tableVal;
				break;
			}
			case userfunc_m:{
				result = rv1->data.funcVal == rv2->data.funcVal;
				break;
			}
			case libfunc_m: {
				int c = strcmp(rv1->data.libfuncVal,rv2->data.libfuncVal);
				if(c == 0)
					result = 1;
				break;
			}
			default: assert(0);
		}
	}
	
	if((!executionFinished) && (!result))
		pc = instr->result.val;
}

void execute_jge(instruction* instr){
	assert(instr -> result.type == label_a);
	
	avm_memcell* rv1 = avm_translate_operand (&instr->arg1, &ax);
	avm_memcell* rv2 = avm_translate_operand (&instr->arg2, &bx);
	
	unsigned char result = 0;

	if(rv1->type == number_m || rv2->type == number_m){
		fprintf(stderr,"Not a number\n");
		executionFinished = 1;
	}
	else{
		result = rv1->data.numVal >= rv2->data.numVal;
	}
	
	if(!executionFinished && result)
		pc = instr->result.val;
}

void execute_jle(instruction* instr){
	assert(instr -> result.type == label_a);
	
	avm_memcell* rv1 = avm_translate_operand (&instr->arg1, &ax);
	avm_memcell* rv2 = avm_translate_operand (&instr->arg2, &bx);
	
	unsigned char result = 0;

	if(rv1->type == number_m || rv2->type == number_m){
		fprintf(stderr,"Not a number\n");
		executionFinished = 1;
	}
	else{
		result = rv1->data.numVal <= rv2->data.numVal;
	}
	
	if(!executionFinished && result)
		pc = instr->result.val;
}

void execute_jgt(instruction* instr){
	assert(instr -> result.type == label_a);
	
	avm_memcell* rv1 = avm_translate_operand (&instr->arg1, &ax);
	avm_memcell* rv2 = avm_translate_operand (&instr->arg2, &bx);
	
	unsigned char result = 0;

	if(rv1->type == number_m || rv2->type == number_m){
		fprintf(stderr,"Not a number\n");
		executionFinished = 1;
	}
	else{
		result = rv1->data.numVal > rv2->data.numVal;
	}
	
	if(!executionFinished && result)
		pc = instr->result.val;
}

void execute_jlt(instruction* instr){
	assert(instr -> result.type == label_a);
	
	avm_memcell* rv1 = avm_translate_operand (&instr->arg1, &ax);
	avm_memcell* rv2 = avm_translate_operand (&instr->arg2, &bx);
	
	unsigned char result = 0;

	if(rv1->type == number_m || rv2->type == number_m){
		fprintf(stderr,"Not a number\n");
		executionFinished = 1;
	}
	else{
		result = rv1->data.numVal < rv2->data.numVal;
	}
	
	if(!executionFinished && result)
		pc = instr->result.val;
}

void execute_jump(instruction* instr){
	pc = instr->result.val;
}