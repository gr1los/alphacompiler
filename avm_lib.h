#define AVM_TABLE_HASHSIZE 211

#define execute_add execute_arithmetic
#define execute_sub execute_arithmetic
#define execute_mul execute_arithmetic
#define execute_div execute_arithmetic
#define execute_mod execute_arithmetic

#ifndef AVM_LIB_H_
#define AVM_LIB_H_

typedef enum avm_memcell_t {
	number_m	=0,
	string_m    =1,
	bool_m	    =2,
	table_m     =3,
	userfunc_m  =4,
	libfunc_m   =5,
	nil_m		=6,
	undef_m		=7
}avm_memcell_t;

struct avm_table;

typedef struct avm_memcell {
	avm_memcell_t type;
	union {
		double numVal;
		char* strVal;
		unsigned char boolVal;
		struct avm_table* tableVal;
		unsigned funcVal;
		char* libfuncVal;
	} data;
}avm_memcell;

typedef struct avm_table_bucket{
	avm_memcell					key;
	avm_memcell 				value;
	struct avm_table_bucket* 	next;
}avm_table_bucket;

typedef struct avm_table{
	unsigned			refCounter;
	avm_table_bucket*	strIndexed[AVM_TABLE_HASHSIZE];
	avm_table_bucket*	numIndexed[AVM_TABLE_HASHSIZE];
	unsigned			total;
}avm_table;

typedef void (*execute_func_t)(instruction*);

void execute_assign (instruction*);

void execute_add (instruction*);
void execute_sub (instruction*);
void execute_mul (instruction*);
void execute_div (instruction*);
void execute_mod (instruction*);
void execute_uminus (instruction*);

void execute_and (instruction*);
void execute_or (instruction*);
void execute_not (instruction*);

void execute_jeq (instruction*);
void execute_jne (instruction*);
void execute_jle (instruction*);
void execute_jge (instruction*);
void execute_jlt (instruction*);
void execute_jgt (instruction*);

extern void execute_call (instruction*);
extern void execute_pusharg (instruction*);
extern void execute_funcenter (instruction*);
extern void execute_funcexit (instruction*);

void execute_newtable (instruction*);
void execute_tablegetelem (instruction*);
void execute_tablesetelem (instruction*);

void execute_nop (instruction*);
void execute_jump (instruction*);

static execute_func_t executeFuncs[]={
	execute_assign,
	execute_add,
	execute_sub,
	execute_mul,
	execute_div,
	execute_mod,
	//execute_uminus,
	//execute_and,
	//execute_or,
	//execute_not,
	0,
	0,
	0,
	0,
	execute_jeq,
	execute_jne,
	execute_jle,
	execute_jge,
	execute_jlt,
	execute_jgt,
	execute_call,
	execute_pusharg,
	execute_funcenter,
	execute_funcexit,
	execute_newtable,
	execute_tablegetelem,
	execute_tablesetelem,
	0,//execute_nop,
	execute_jump
};


double consts_getnumber (unsigned);
char* consts_getstring (unsigned);
char* libfuncs_getused (unsigned);

avm_memcell* avm_translate_operand(vmarg*, avm_memcell*);

unsigned execute_cycle(void);
void execute_code(void);

void avm_memcellclear(avm_memcell*);
void memclear_string(avm_memcell*);
void memclear_table(avm_memcell*);

typedef void (*memclear_func_t)(avm_memcell*);
static memclear_func_t memclearFuncs[] = {
	0,					// number
	memclear_string,
	0,					// bool
	memclear_table,
	0,					// userfunc
	0,					// libfunc
	0,					// nil
	0					// undef
};

void			avm_tablebucketsinit(avm_table_bucket**);
avm_table*		avm_tablenew(void);
void			avm_tablebucketsdestroy(avm_table_bucket**);
void			avm_tabledestroy(avm_table*);
avm_memcell*	avm_tablegetelem(avm_table*,avm_memcell*);
void			avm_tablesetelem(avm_table*,avm_memcell*, avm_memcell*);
void			avm_initstack(void);
void			avm_tableincrefcounter(avm_table*);
void			avm_tabledecrefcounter(avm_table*);

// AVM INSTRUCTION FUNCTIONS
void			avm_assign(avm_memcell*, avm_memcell*);
void			avm_registerlibfunc(char* id);


// EXECUTE INSTRUCTION FUNCTIONS
void 			execute_assign(instruction*);

#endif
