all: flexer parser
	gcc -g -o al.out al.c parser.c hash_table.c quads_lib.c avm_lib.c arithmetic_im.c func_im.c eq_im.c table_im.c

debug: flexer parser
	gcc -g -o al.out al.c parser.c

flexer: al.l
	flex --outfile=al.c al.l

parser: parser.y
	bison --debug --yacc --verbose --defines --output=parser.c parser.y

symtable:
	gcc -g hash_table.c -o hash_table.o

quads:
	gcc -g quads_lib.c -o quads_lib.o

clean:
	rm -rf al.c
	rm -rf parser.c
	rm -rf parser.h
	rm -rf al.out
	rm -rf hash_table.out
	rm -rf quads_lib.out
	rm -rf parser.output
